SELECT * FROM sso_menu;
SELECT * FROM sso_role_menu;
SELECT * FROM sso_role_system;
SELECT * FROM sso_system;

-- 删除一个系统
DELETE FROM sso_menu WHERE system_code='dms';
DELETE FROM sso_role_menu WHERE menu_system_code='dms';
DELETE FROM sso_role_system WHERE system_code='dms';
DELETE FROM sso_system WHERE `code`='dms';

-- 修改一个系统编码
UPDATE sso_menu SET system_code='dunwu'  WHERE system_code='sso';
UPDATE sso_role_menu SET menu_system_code='dunwu'  WHERE menu_system_code='sso';
UPDATE sso_role_system SET system_code='dunwu'  WHERE system_code='sso';
UPDATE sso_system SET `code`='dunwu'  WHERE `code`='sso';

-- 修改菜单编码
UPDATE sso_menu SET `code`='order_man' WHERE `code`='div_doc';
UPDATE sso_menu SET `code`='order_set',parent_code='order_man' WHERE `code`='doc_1';
UPDATE sso_role_menu SET menu_code='order_man' WHERE menu_code='div_doc';
UPDATE sso_role_menu SET menu_code='order_set' WHERE menu_code='doc_1';
