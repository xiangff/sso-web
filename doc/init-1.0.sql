
CREATE DATABASE sso DEFAULT CHARSET utf8;

USE sso;

-- 用户表 --
CREATE TABLE sso_user
(
id	VARCHAR(50) PRIMARY KEY	COMMENT "	主键 uuid	",
account	VARCHAR(50)	COMMENT "	账号,唯一	",
`name`	VARCHAR(50)	COMMENT "	昵称	",
pwd	VARCHAR(50)	COMMENT "	密码	",
role_code	VARCHAR(50)	COMMENT "	角色编号	",
phone	VARCHAR(20)	COMMENT "	手机	",
email	VARCHAR(100)	COMMENT "	邮箱	",
idcard	VARCHAR(50)	COMMENT "	身份证号码	",
txurl	VARCHAR(500)	COMMENT "	头像地址	",
qq	VARCHAR(20)	COMMENT "	QQ号	",
weixin	VARCHAR(20)	COMMENT "	微信号	",
is_delete	CHAR(1)	COMMENT "	是否允许删除 : Y/N	",
is_enable	CHAR(1)	COMMENT "	是否允许登录 : Y/N	",
active	CHAR(1)	COMMENT "	是否有效 : Y/N	",
create_time	DATETIME	COMMENT "	创建时间	",
create_user	VARCHAR(20)	COMMENT "	创建人	",
update_time	DATETIME	COMMENT "	修改时间	",
update_user	VARCHAR(20)	COMMENT "	修改人	",
skin	VARCHAR(10)	COMMENT "	界面主题	",
 UNIQUE KEY `unique_code` (`account`)
)DEFAULT CHARSET utf8 COMMENT='用户表';

-- 角色表(sso_role) --
CREATE TABLE sso_role
(
id	VARCHAR(50)  PRIMARY KEY	COMMENT "	主键 uuid	",
`code`	VARCHAR(50)	COMMENT "	角色编号 唯一	",
`name`	VARCHAR(50)	COMMENT "	角色名称	",
grade	INT	COMMENT "	角色等级	",
remark	VARCHAR(500)	COMMENT "	备注	",
notdele	CHAR(1)	COMMENT "	是否允许删除 : Y/N	",
active	CHAR(1)	COMMENT "	是否有效 : Y/N	",
create_time	DATETIME	COMMENT "	创建时间	",
create_user	VARCHAR(20)	COMMENT "	创建人	",
update_time	DATETIME	COMMENT "	修改时间	",
update_user	VARCHAR(20)	COMMENT "	修改人	",
UNIQUE KEY `unique_code` (`code`)
)DEFAULT CHARSET utf8 COMMENT='角色表';

-- 菜单表(sso_menu)
CREATE TABLE sso_menu
(
id	VARCHAR(50)  PRIMARY KEY	COMMENT "	主键 uuid	",
system_code	VARCHAR(50)	COMMENT "	系统编号,联合主键	",
`code`	VARCHAR(50)	COMMENT "	菜单编号,联合主键	",
`name`	VARCHAR(50)	COMMENT "	菜单名称	",
grade	INT	COMMENT "	菜单等级	",
parent_code	VARCHAR(50)	COMMENT "	父菜单编号	",
icon	VARCHAR(100)	COMMENT "	图标	",
url	VARCHAR(500)	COMMENT "	链接	",
`sequence`	INT	COMMENT "	顺序(根据级别)	",
notdele	CHAR(1)	COMMENT "	是否允许删除 : Y/N	",
is_enable	CHAR(1)	COMMENT "	是否启用 : Y/N	",
create_time	DATETIME	COMMENT "	创建时间	",
create_user	VARCHAR(20)	COMMENT "	创建人	",
update_time	DATETIME	COMMENT "	修改时间	",
update_user	VARCHAR(20)	COMMENT "	修改人	",
UNIQUE KEY `unique_code` (system_code,`code`)
)DEFAULT CHARSET utf8 COMMENT='菜单表';

-- 系统表(sso_system)
CREATE TABLE sso_system
(
id	VARCHAR(50)  PRIMARY KEY	COMMENT "	主键 uuid	",
`code`	VARCHAR(50)	COMMENT "	系统编号 唯一	",
`name`	VARCHAR(50)	COMMENT "	系统名称	",
url	INT	COMMENT "	系统链接	",
icon	VARCHAR(500)	COMMENT "	系统图标	",
remark	VARCHAR(500)	COMMENT "	备注	",
`sequence`	INT	COMMENT "	顺序(根据级别)	",
active	CHAR(1)	COMMENT "	是否有效 : Y/N	",
create_time	DATETIME	COMMENT "	创建时间	",
create_user	VARCHAR(20)	COMMENT "	创建人	",
update_time	DATETIME	COMMENT "	修改时间	",
update_user	VARCHAR(20)	COMMENT "	修改人	",
UNIQUE KEY `unique_code` (`code`)
)DEFAULT CHARSET utf8 COMMENT='系统表';

-- 用户角色关系表(sso_user_role) --
CREATE TABLE sso_user_role
(
user_account	VARCHAR(50)	COMMENT "	用户账号	",
role_code	VARCHAR(50)	COMMENT "	角色编号	"
)DEFAULT CHARSET utf8 COMMENT='用户角色关系表';

-- 角色菜单关系表(sso_role_menu)
CREATE TABLE sso_role_menu
(
role_code	VARCHAR(50)	COMMENT "	角色编号	",
menu_code	VARCHAR(50)	COMMENT "	菜单编号	"
)DEFAULT CHARSET utf8 COMMENT='角色菜单关系表';

-- 角色系统关系表(sso_role_system)
CREATE TABLE sso_role_system
(
role_code	VARCHAR(50)	COMMENT "	角色编号	",
system_code	VARCHAR(50)	COMMENT "	系统编号	"
)DEFAULT CHARSET utf8 COMMENT='角色系统关系表';

-- 系统数据字典类型表(sys_data_dic_type)
CREATE TABLE sys_data_dic_type
(
id	INT  PRIMARY KEY	COMMENT "	主键 自增	",
type_code	VARCHAR(50)	COMMENT "	类型code,唯一	",
`name`	VARCHAR(50)	COMMENT "	类型名称	",
UNIQUE KEY `unique_code` (`type_code`)
)DEFAULT CHARSET utf8 COMMENT='系统数据字典类型表';

-- 系统数据字典表(sys_data_dic)
CREATE TABLE sys_data_dic
(
id	VARCHAR(50)  PRIMARY KEY	COMMENT "	主键 uuid	",
type_code	VARCHAR(50)	COMMENT "	类型编号(sys_data_dic_type.type_code)	",
`name`	VARCHAR(50)	COMMENT "	名称	",
`value`	VARCHAR(50)	COMMENT "	值	",
active	CHAR(1)	COMMENT "	是否有效 : Y/N	",
create_time	DATETIME	COMMENT "	创建时间	",
create_user	VARCHAR(20)	COMMENT "	创建人	",
update_time	DATETIME	COMMENT "	修改时间	",
update_user	VARCHAR(20)	COMMENT "	修改人	"
)DEFAULT CHARSET utf8 COMMENT='系统数据字典表';