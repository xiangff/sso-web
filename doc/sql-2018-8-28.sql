/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.6.40 : Database - sso
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sso` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sso`;

/*Table structure for table `sso_data_dic` */

DROP TABLE IF EXISTS `sso_data_dic`;

CREATE TABLE `sso_data_dic` (
  `id` varchar(50) NOT NULL COMMENT '	主键 uuid	',
  `type_code` varchar(50) DEFAULT NULL COMMENT '	类型编号(sys_data_dic_type.type_code)	',
  `name` varchar(50) DEFAULT NULL COMMENT '	名称	',
  `value` varchar(50) DEFAULT NULL COMMENT '	值	',
  `active` char(1) DEFAULT NULL COMMENT '	是否有效 : Y/N	',
  `create_time` datetime DEFAULT NULL COMMENT '	创建时间	',
  `create_user` varchar(20) DEFAULT NULL COMMENT '	创建人	',
  `update_time` datetime DEFAULT NULL COMMENT '	修改时间	',
  `update_user` varchar(20) DEFAULT NULL COMMENT '	修改人	',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统数据字典表';

/*Table structure for table `sso_data_dic_type` */

DROP TABLE IF EXISTS `sso_data_dic_type`;

CREATE TABLE `sso_data_dic_type` (
  `id` int(11) NOT NULL COMMENT '	主键 自增	',
  `type_code` varchar(50) DEFAULT NULL COMMENT '	类型code,唯一	',
  `name` varchar(50) DEFAULT NULL COMMENT '	类型名称	',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统数据字典类型表';

/*Table structure for table `sso_menu` */

DROP TABLE IF EXISTS `sso_menu`;

CREATE TABLE `sso_menu` (
  `id` varchar(50) NOT NULL COMMENT '	主键 uuid	',
  `system_code` varchar(50) DEFAULT NULL COMMENT '	系统编号,联合主键	',
  `code` varchar(50) DEFAULT NULL COMMENT '	菜单编号,联合主键	',
  `name` varchar(50) DEFAULT NULL COMMENT '	菜单名称	',
  `grade` int(11) DEFAULT NULL COMMENT '	菜单等级	',
  `parent_code` varchar(50) DEFAULT NULL COMMENT '	父菜单编号	',
  `icon` varchar(100) DEFAULT NULL COMMENT '	图标	',
  `url` varchar(500) DEFAULT NULL COMMENT '	链接	',
  `sequence` int(11) DEFAULT NULL COMMENT '	顺序(根据级别)	',
  `notdele` char(1) DEFAULT NULL COMMENT '	是否允许删除 : Y/N	',
  `is_enable` char(1) DEFAULT NULL COMMENT '	是否启用 : Y/N	',
  `create_time` datetime DEFAULT NULL COMMENT '	创建时间	',
  `create_user` varchar(20) DEFAULT NULL COMMENT '	创建人	',
  `update_time` datetime DEFAULT NULL COMMENT '	修改时间	',
  `update_user` varchar(20) DEFAULT NULL COMMENT '	修改人	',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`system_code`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

/*Table structure for table `sso_role` */

DROP TABLE IF EXISTS `sso_role`;

CREATE TABLE `sso_role` (
  `id` varchar(50) NOT NULL COMMENT '	主键 uuid	',
  `code` varchar(50) DEFAULT NULL COMMENT '	角色编号 唯一	',
  `name` varchar(50) DEFAULT NULL COMMENT '	角色名称	',
  `grade` int(11) DEFAULT NULL COMMENT '	角色等级	',
  `remark` varchar(500) DEFAULT NULL COMMENT '	备注	',
  `notdele` char(1) DEFAULT NULL COMMENT '	是否允许删除 : Y/N	',
  `active` char(1) DEFAULT NULL COMMENT '	是否有效 : Y/N	',
  `create_time` datetime DEFAULT NULL COMMENT '	创建时间	',
  `create_user` varchar(20) DEFAULT NULL COMMENT '	创建人	',
  `update_time` datetime DEFAULT NULL COMMENT '	修改时间	',
  `update_user` varchar(20) DEFAULT NULL COMMENT '	修改人	',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

/*Table structure for table `sso_role_menu` */

DROP TABLE IF EXISTS `sso_role_menu`;

CREATE TABLE `sso_role_menu` (
  `role_code` varchar(50) DEFAULT NULL COMMENT '	角色编号	',
  `menu_code` varchar(50) DEFAULT NULL COMMENT '	菜单编号	',
  `menu_system_code` varchar(50) DEFAULT NULL COMMENT '系统编号,用来定位menu_code'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色菜单关系表';

/*Table structure for table `sso_role_system` */

DROP TABLE IF EXISTS `sso_role_system`;

CREATE TABLE `sso_role_system` (
  `role_code` varchar(50) DEFAULT NULL COMMENT '	角色编号	',
  `system_code` varchar(50) DEFAULT NULL COMMENT '	系统编号	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色系统关系表';

/*Table structure for table `sso_system` */

DROP TABLE IF EXISTS `sso_system`;

CREATE TABLE `sso_system` (
  `id` varchar(50) NOT NULL COMMENT '	主键 uuid	',
  `code` varchar(50) DEFAULT NULL COMMENT '	系统编号 唯一	',
  `name` varchar(50) DEFAULT NULL COMMENT '	系统名称	',
  `url` varchar(500) DEFAULT NULL COMMENT '	系统链接	',
  `icon` varchar(500) DEFAULT NULL COMMENT '	系统图标	',
  `remark` varchar(500) DEFAULT NULL COMMENT '	备注	',
  `sequence` int(11) DEFAULT NULL COMMENT '	顺序(根据级别)	',
  `active` char(1) DEFAULT NULL COMMENT '	是否有效 : Y/N	',
  `create_time` datetime DEFAULT NULL COMMENT '	创建时间	',
  `create_user` varchar(20) DEFAULT NULL COMMENT '	创建人	',
  `update_time` datetime DEFAULT NULL COMMENT '	修改时间	',
  `update_user` varchar(20) DEFAULT NULL COMMENT '	修改人	',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统表';

/*Table structure for table `sso_user` */

DROP TABLE IF EXISTS `sso_user`;

CREATE TABLE `sso_user` (
  `id` varchar(50) NOT NULL COMMENT '	主键 uuid	',
  `account` varchar(50) DEFAULT NULL COMMENT '	账号,唯一	',
  `name` varchar(50) DEFAULT NULL COMMENT '	昵称	',
  `pwd` varchar(50) DEFAULT NULL COMMENT '	密码	',
  `role_code` varchar(50) DEFAULT NULL COMMENT '	角色编号	',
  `sex` char(1) DEFAULT NULL COMMENT '性别: 1 男,2 女,3 未知',
  `phone` varchar(20) DEFAULT NULL COMMENT '	手机	',
  `email` varchar(100) DEFAULT NULL COMMENT '	邮箱	',
  `idcard` varchar(50) DEFAULT NULL COMMENT '	身份证号码	',
  `txurl` varchar(500) DEFAULT NULL COMMENT '	头像地址	',
  `qq` varchar(20) DEFAULT NULL COMMENT '	QQ号	',
  `weixin` varchar(20) DEFAULT NULL COMMENT '	微信号	',
  `is_delete` char(1) DEFAULT NULL COMMENT '	是否允许删除 : Y/N	',
  `is_enable` char(1) DEFAULT NULL COMMENT '	是否允许登录 : Y/N	',
  `active` char(1) DEFAULT NULL COMMENT '	是否有效 : Y/N	',
  `create_time` datetime DEFAULT NULL COMMENT '	创建时间	',
  `create_user` varchar(20) DEFAULT NULL COMMENT '	创建人	',
  `update_time` datetime DEFAULT NULL COMMENT '	修改时间	',
  `update_user` varchar(20) DEFAULT NULL COMMENT '	修改人	',
  `skin` varchar(10) DEFAULT NULL COMMENT '	界面主题	',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Table structure for table `sso_user_role` */

DROP TABLE IF EXISTS `sso_user_role`;

CREATE TABLE `sso_user_role` (
  `user_account` varchar(50) DEFAULT NULL COMMENT '	用户账号	',
  `role_code` varchar(50) DEFAULT NULL COMMENT '	角色编号	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色关系表';

/*Table structure for table `test` */

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` int(11) NOT NULL COMMENT '主键',
  `name` varchar(20) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `aaa` bigint(20) DEFAULT NULL,
  `bb` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
