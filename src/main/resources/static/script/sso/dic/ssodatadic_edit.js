    layui.use(['element', 'form', 'layer', 'verify' , 'web'], function () {
        var element = layui.element;
        var form = layui.form;
        var layer = layui.layer;
        var web = layui.web;

        var contro_path = '/ssodatadic';
        //…

        var id = $("#id").val();
        var obj = null;
        if (id != null && id != "") {
            var url = basePath + contro_path + "/searchDetail";
            web.load();
            web.ajax_sync(url, 'post', {id: id}, function (data) {
                obj = data.result;
                $("input[name='typeCode']").val(obj.typeCode);
                $("input[name='name']").val(obj.name);
                $("input[name='value']").val(obj.value);
                $("input[name='active']").val(obj.active);
                $("input[name='createTime']").val(obj.createTime);
                $("input[name='createUser']").val(obj.createUser);
                $("input[name='updateTime']").val(obj.updateTime);
                $("input[name='updateUser']").val(obj.updateUser);
                form.render();
            });
        }

        //关闭当前弹出层
        $("#close").click(function () {
            var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层
        });
        //保存
        form.on('submit(save)', function (data) {
            var url = basePath + contro_path + "/save";
            console.log(data.field);
            web.ajax(url, 'post', data.field, function (data) {
                layer.alert("保存成功", {
                    icon: 1
                }, function (index) {
                    layer.close(index);//关闭当前层

                    var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                    parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层

                    parent.$("#query_data").click();
                });
            });
            return false;
        });

    });