    layui.use(['element', 'form', 'layer', 'verify' , 'web'], function () {
        var element = layui.element;
        var form = layui.form;
        var layer = layui.layer;
        var web = layui.web;

        var contro_path = '/ssodatadic';
        //…
        $(".layui-input")[0].focus();

        //关闭当前弹出层
        $("#close").click(function () {
            var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层
        });
        //保存
        form.on('submit(save)', function (data) {
            var url = basePath + contro_path + "/save";
            var edit_data = data.field;
            //console.log(data.field);
            web.load();
            web.ajax(url, 'post', edit_data, function (data) {
                layer.alert("保存成功", {
                    icon : 1
                }, function(index) {
                    layer.close(index);//关闭当前层

                    var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                    parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层

                    parent.$("#query_data").click();
                });
            });
            return false;
        });

    });
