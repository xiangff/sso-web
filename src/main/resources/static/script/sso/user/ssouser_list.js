layui.use(['element', 'form', 'layer', 'table', 'laypage', 'web', 'user'], function () {
    var element = layui.element;
    var form = layui.form;
    var layer = layui.layer;
    var table = layui.table;
    var web = layui.web;
    var user = layui.user;

    var table_list_id = 'table_list_id'; // 表格id
    var table_list_tool = "table_list_tool"; //表格工具
    var contro_path = '/ssouser'; //controller 的路径

    // 角色下拉框
    var role_url = basePath + "/ssorole/listAll";
    web.ajax(role_url, 'post', {}, function (data) {
        var arr = data.result;
        var html = "<option value=''>--全部--</option>";
        if (arr.length > 0) {
            for (var i = 0; i < arr.length; i++) {
                html += '<option value="' + arr[i].code + '">' + arr[i].name + '</option>';
            }
            sys_code_flag = false;
        } else {
            html = '<option value="">无系统数据</option>';
        }
        $("#roleCode").html(html);
        form.render('select');
    });

    //查询数据
    $("#query_data").click(function () {
        web.load();
        web.page_query("#", "#", function (list) {
            table.render({
                elem: '#' + table_list_id
                , skin: 'line,row' //行,列边框
                , even: true //开启隔行背景
                , height: table_height
                , data: list
                , cols: [
                    [
                        {type: 'checkbox'}
                        , {field: 'index', title: '序号', width: 70, templet: '<div>{{ d.LAY_INDEX }}</div>'}
                        , {field: 'account', title: '用户账号', width: 150}
                        , {field: 'txurl', title: '头像', templet: tpl_img, width: 80}
                        , {field: 'name', title: '昵称', width: 150}
                        , {field: 'sex', title: '性别', templet: tpl_sex, width: 80}
                        , {field: 'phone', title: '手机', width: 150}
                        , {field: 'email', title: '邮箱', width: 170}
                        , {field: 'idcard', title: '身份证号码', width: 170}
                        , {field: 'qq', title: 'QQ号', width: 150}
                        , {field: 'weixin', title: '微信号', width: 150}
                        , {field: 'isEnable', title: '登录', templet: tpl_isEnable, width: 80}
                        , {field: 'createTime', title: '创建时间', templet: tpl_createTime, width: 160}
                        , {field: 'createUser', title: '创建人', width: 150}
                        , {field: 'updateTime', title: '修改时间', templet: tpl_updateTime, width: 160}
                        , {field: 'updateUser', title: '修改人', width: 150}
                        , {fixed: 'right', title: '操作', width: 150, align: 'left', toolbar: '#list_tool_tpl'}
                    ]
                ]
            });
            user.checkAuth();
            layer.photos({
                photos: '.layer-photos-demo'
                , anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
            });
        });
    });

    function tpl_img(d) {
        var html = '<div class="layer-photos-demo">' +
            '<img width="35px;" height="35px;"   layer-pid="' + d.id + '" layer-src="' + d.txurl + '" src="' + d.txurl + '" alt="' + d.name + '" />' +
            '</div>';
        return html;
    }

    function tpl_isEnable(d) {
        var html = "";
        if (d.isEnable == "Y") {
            html = '<input type="checkbox" lay-filter="isEnable" lay-skin="switch" lay-text="ON|OFF" value="' + d.id + '" checked>';
        } else {
            html = '<input type="checkbox" lay-filter="isEnable" lay-skin="switch" lay-text="ON|OFF" value="' + d.id + '" >';
        }
        return html;
    }

    form.on('switch(isEnable)', function (data) {
        var isEnable = data.elem.checked ? 'Y' : 'N';
        var id = data.value;
        var data = {id: id, isEnable: isEnable};
        var url = basePath + contro_path + "/save";
        web.ajax(url, 'post', data, function (res) {
            layer.alert("修改成功!");
        });
    });

    //监听工具条==>操作
    table.on('tool(' + table_list_tool + ')', function (obj) {
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的DOM对象
        //查看
        if (layEvent === 'detail') {
            //do somehing
        }
        //删除
        else if (layEvent === 'del') {
            web.delete_select(contro_path, data.id, function () {
                obj.del();
            });
        }
        //编辑
        else if (layEvent === 'edit') {
            show_edit(data.id);
        }

    });
    /**
     * '全部删除' 按钮
     */
    $("#delete_all").click(function () {
        web.delete_select_all(contro_path, table_list_id, function () {
            $("#query_data").click();
        });
    });
    /*----------------------------------------------------------
            弹出层
    ------------------------------------------------------------ */
    var ifame_height = '700px'; //弹出层的高度
    var title_name = '用户信息';


    //添加
    $("#add").click(function () {
        var index_add = layer.open({
            title: ['新增' + title_name, font_set]
            , type: 2
            , fixed: false
            , maxmin: true
            , area: ['600px', ifame_height]     //宽,高
            , offset: ['50px', '100px']    // top,left
            , content: web.url_replace('sso/user/ssouser_add')
        });
    });

    //编辑
    function show_edit(id) {
        var index_edit = layer.open({
            title: ['编辑' + title_name, font_set]
            , type: 2
            , fixed: false
            , maxmin: true
            , area: ['600px', ifame_height] //宽,高
            , offset: ['50px', '100px']// top,left
            , content: web.url_replace('sso/user/ssouser_edit?id=' + id)
        });
    }

    $("#user_roleSet").click(function () {
        web.select_one_checkbox(table_list_id, function (data) {
            var index_op = layer.open({
                title: ['编辑 <b>' + data.account+'</b> 的角色配置', font_set]
                , type: 2
                , fixed: false
                , maxmin: true
                , area: ['600px', '600px'] //宽,高
                , offset: ['100px', '100px']// top,left
                , content: web.url_replace('sso/tree/role?account=' + data.account)
            });
        });
    });
});