layui.use(['element', 'form', 'layer', 'verify', 'web', 'upload', 'user'], function () {
    var element = layui.element;
    var form = layui.form;
    var layer = layui.layer;
    var web = layui.web;
    var user = layui.user;
    var upload = layui.upload;

    var contro_path = '/ssouser';
    var id = $("#id").val();
    var account = "";
    //…
    $(".layui-input")[0].focus();

    // 加载数据
    web.load();
    web.ajax_sync(basePath + contro_path + "/searchDetail", 'post', {id: id}, function (data) {
        obj = data.result;
        account = obj.account;
        $("input[name='account']").val(obj.account);
        $("input[name='name']").val(obj.name);
        $("input[name='phone']").val(obj.phone);
        $("input[name='email']").val(obj.email);
        $("input[name='idcard']").val(obj.idcard);
        $("input[name='qq']").val(obj.qq);
        $("input[name='weixin']").val(obj.weixin);
        $("input[name='txurl']").val(obj.txurl);
        $(".uploadImg").attr("src", obj.txurl);

        $("input[lay-filter='isEnable']").attr("checked", obj.isEnable == 'Y');
        $("input[name='isEnable']").val(obj.isEnable);

        $("input[name='sex']").each(function () {
            if (this.value == obj.sex) {
                $(this).get(0).checked = true;
            }
        });

        form.render();
    });
    var uploadInst = upload.render({
        elem: '.uploadImg'
        , url: upload_url
        , data: {projectName: upload_project_name, moduleName: 'user'}
        , accept: 'file'
        , ext: 'jpg|png'
        , done: function (res) {
            if (res.success) {
                var src = res.result.src;
                $(".uploadImg").attr("src", src);
                $("#txurl").val(src);
            } else {
                layer.alert("图片上传失败! \n " + res.message);
            }
        }
        , error: function () {
            layer.alert("图片上传失败!");
        }
    });

    //关闭当前弹出层
    $("#close").click(function () {
        var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层
    });
    form.verify({
        uniqueCode: function (value, item) {
            if (value == account) {
                return;
            }
            var url = basePath + contro_path + "/searchDetail";
            var res = "";
            web.ajax_sync(url, 'post', {account: value}, function (data) {
                if (data.result != null) {
                    res = data.result.account;
                }
            });
            if (res.length > 0) {
                return ' ' + value + '  已被注册';
            }
        }
    });
    form.on('switch(isEnable)', function (data) {
        var isEnable = data.elem.checked ? 'Y' : 'N';
        $("#isEnable").val(isEnable);
    });
    //保存
    form.on('submit(save)', function (data) {
        var url = basePath + contro_path + "/save";
        var edit_data = data.field;
        edit_data.id = id;
        web.load();
        web.ajax(url, 'post', edit_data, function (data) {
            layer.alert("保存成功", {
                icon: 1
            }, function (index) {
                layer.close(index);//关闭当前层

                var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层

                if (account == user.getUser().account) {
                    user.cleanUser();
                    web.ajax(basePath+"/ssouser/cleanUserCache", 'post', {account: account}, function () {
                        user.initData();
                    });
                }

                parent.$("#query_data").click();
            });
        });
        return false;
    });

});