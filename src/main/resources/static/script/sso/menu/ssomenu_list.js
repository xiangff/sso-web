layui.use(['element', 'form', 'layer', 'table', 'laypage', 'web', 'user'], function () {
    var element = layui.element;
    var form = layui.form;
    var layer = layui.layer;
    var table = layui.table;
    var web = layui.web;
    var user = layui.user;

    var table_list_id = 'table_list_id'; // 表格id
    var table_list_tool = "table_list_tool"; //表格工具
    var contro_path = '/ssomenu'; //controller 的路径

    // 加载所有的系统
    var sys_code_flag = true;
    web.ajax_sync(basePath + "/ssosystem/list", 'post', {}, function (data) {
        var arr = data.result;
        var html = "";
        if (arr.length > 0) {
            for (var i = 0; i < arr.length; i++) {
                html += '<option value="' + arr[i].code + '">' + arr[i].name + '</option>';
            }
            sys_code_flag = false;
        } else {
            html = '<option value="">无系统数据</option>';
        }
        $("#systemCode").html(html);
        form.render('select');
    });

    //查询数据
    $("#query_data").click(function () {
        if (sys_code_flag) {
            layer.alert("没有系统数据,无法查询");
            return;
        }
        web.load();
        web.page_query("#", "#", function (list) {
            table.render({
                elem: '#' + table_list_id
                , skin: 'line,row' //行,列边框
                , even: true //开启隔行背景
                , height: table_height
                , data: list
                , cols: [
                    [
                        {type: 'checkbox'}
                        , {field: 'index', title: '序号', width: 70, templet: '<div>{{ d.LAY_INDEX }}</div>'}
                        , {field: 'code', title: '菜单编号', width: 160}
                        , {field: 'name', title: '菜单名称 (个数)', templet: tpl_menuName, width: 160}
                        , {field: 'grade', title: '等级'}
                        , {field: 'parentCode', title: '父菜单编号', width: 160}
                        , {field: 'url', title: '链接', width: 250}
                        , {field: 'sequence', title: '顺序'}
                        , {field: 'authKey', title: 'authKey', width: 150}
                        , {field: 'isEnable', title: '启用', templet: tpl_isEnable, width: 80}
                        , {field: 'createTime', title: '创建时间', templet: tpl_createTime, width: 160}
                        , {field: 'createUser', title: '创建人', width: 150}
                        , {field: 'updateTime', title: '修改时间', templet: tpl_updateTime, width: 160}
                        , {field: 'updateUser', title: '修改人', width: 150}
                        , {fixed: 'right', title: '操作', width: 150, align: 'left', toolbar: '#list_tool_tpl'}
                    ]
                ]
            });
            user.checkAuth();
        });
    });

    // 页面加载是执行查询
    $("#query_data").click();

    function tpl_isEnable(d) {
        var html = "";
        if (d.isEnable == "Y") {
            html = '<input type="checkbox" lay-filter="isEnable" lay-skin="switch" lay-text="ON|OFF" value="' + d.id + '" checked>';
        } else {
            html = '<input type="checkbox" lay-filter="isEnable" lay-skin="switch" lay-text="ON|OFF" value="' + d.id + '" >';
        }
        return html;
    }

    form.on('switch(isEnable)', function (data) {
        var isEnable = data.elem.checked ? 'Y' : 'N';
        var id = data.value;
        var data = {id: id, isEnable: isEnable};
        var url = basePath + contro_path + "/updateState";
        web.load();
        web.ajax(url, 'post', data, function (res) {
            layer.alert("修改成功!");
        });
    });

    // 菜单名字
    function tpl_menuName(d) {
        var html = '';
        if (d.grade < 3) {
            html += '<i class="layui-icon menu">' + d.icon + '</i>&nbsp;';//replace("&amp;", "&")
            html += '<a href="#" class="menuName" name="' + d.code + '" parentName="' + d.name + '">' + d.name + '</a>';
        } else {
            html += d.name;
        }
        html += '&nbsp;&nbsp;<span style="color: red">'+d.count+'</span>';
        return html;
    }

    //点击菜单
    $(document).on("click", ".menuName", function () {
        $("#parentCode").val($(this).attr("name"));
        $("#grade").val(parseInt($("#grade").val()) + 1);
        var parentName = $("#parentCodeName").html();

        if (parentName == '') {
            $("#parentCodeName").html($(this).attr("parentName"));
        } else {
            $("#parentCodeName").html(parentName + " > " + $(this).attr("parentName"));
        }
        //$("#parentCodeName").show();

        form.render('select');
        $("#query_data").click();
    });

    //监听工具条==>操作
    table.on('tool(' + table_list_tool + ')', function (obj) {
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的DOM对象
        //查看
        if (layEvent === 'detail') {
            //do somehing
        }
        //删除
        else if (layEvent === 'del') {
            web.delete_select(contro_path, data.id, function () {
                obj.del();
            });
        }
        //编辑
        else if (layEvent === 'edit') {
            show_edit(data.id);
        }

    });
    /**
     * '全部删除' 按钮
     */
    $("#delete_all").click(function () {
        web.delete_select_all(contro_path, table_list_id, function () {
            $("#query_data").click();
        });
    });
    /*----------------------------------------------------------
            弹出层
    ------------------------------------------------------------ */
    var ifame_height = '700px'; //弹出层的高度
    var title_name = '系统菜单';


    //添加
    $("#add").click(function () {
        var index_add = layer.open({
            title: ['新增' + title_name, font_set]
            , type: 2
            , fixed: false
            , maxmin: true
            , area: ['1000px', ifame_height] //宽,高
            , offset: ['50px', '100px']    // top,left
            , content: web.url_replace('sso/menu/ssomenu_add?systemCode='+$("#systemCode").val())
        });
    });

    //编辑
    function show_edit(id) {
        var index_edit = layer.open({
            title: ['编辑' + title_name, font_set]
            , type: 2
            , fixed: false
            , maxmin: true
            , area: ['1000px', ifame_height] //宽,高
            , offset: ['50px', '100px']// top,left
            , content: web.url_replace('sso/menu/ssomenu_edit?id=' + id)
        });
    }

});