layui.use(['element', 'form', 'layer', 'verify', 'web'], function () {
    var element = layui.element;
    var form = layui.form;
    var layer = layui.layer;
    var web = layui.web;

    var contro_path = '/ssomenu';
    var icon1 = '&#xe614;'
    var icon2 = '&#xe641;'
    $("#icon").val(icon1);
    $("#select_icon").html(icon1);
    //…
    $(".layui-input")[0].focus();

    // 加载所有的系统
    var sys_code_flag = true;
    web.ajax_sync(basePath + "/ssosystem/list", 'post', {}, function (data) {
        var arr = data.result;
        var html = "";
        if (arr.length > 0) {
            for (var i = 0; i < arr.length; i++) {
                html += '<option value="' + arr[i].code + '">' + arr[i].name + '</option>';
            }
            sys_code_flag = false;
        } else {
            html = '<option value="">无系统数据</option>';
        }
        $("#systemCode").html(html);
        var systemCode = $("#systemCode_id").val();
        if (systemCode != null && systemCode.length > 0) {
            $("#systemCode").val(systemCode);
        }
        form.render('select');
    });
    form.on('switch(isEnable)', function (data) {
        if (data.elem.checked) {
            $("#isEnable").val("Y");
        } else {
            $("#isEnable").val("N");
        }
    });
    //选择系统
    form.on('select(systemCode)', function (data) {
        if (sys_code_flag) {
            layer.alert("没有系统数据,无法查询");
            return;
        }
        var grade = parseInt($("#grade").val());
        searpmenu(grade - 1,false);
    });
    //选择菜单等级
    form.on('select(grade)', function (data) {
        if (sys_code_flag) {
            layer.alert("没有系统数据,无法查询");
            return;
        }
        var grade = parseInt(data.value);

        //如果是2级菜单,可以添加key
        if (grade == 2) {
            var all = '<li><input type="text" name="" class="zijiedian_name" value="查询" />&nbsp;<input type="text" name="" class="zijiedian_key" value="_search"/>&nbsp;<img class="butdelete zijiedian_delete"  src="' + basePath + '/istyle/images/delete.png" /></li>';
            all += '<li><input type="text" name="" class="zijiedian_name" value="新增" />&nbsp;<input type="text" name="" class="zijiedian_key" value="_add"/>&nbsp;<img class="butdelete zijiedian_delete"  src="' + basePath + '/istyle/images/delete.png" /></li>';
            all += '<li><input type="text" name="" class="zijiedian_name" value="修改" />&nbsp;<input type="text" name="" class="zijiedian_key" value="_update"/>&nbsp;<img class="butdelete zijiedian_delete"  src="' + basePath + '/istyle/images/delete.png" /></li>';
            all += '<li><input type="text" name="" class="zijiedian_name" value="删除" />&nbsp;<input type="text" name="" class="zijiedian_key" value="_delete"/>&nbsp;<img class="butdelete zijiedian_delete"  src="' + basePath + '/istyle/images/delete.png" /></li>';
            $("#zijiedian").html(all);
            $(".zijiedian_parent").show();
        } else {
            $(".zijiedian_parent").hide();
        }
        form.render('select');

        searpmenu(grade - 1);

    });
    //点击选择图标
    $("#select_icon").click(function () {
        var indes = layer.open({
            title: ['内置图标一览表', 'font-size:18px;'],
            type: 2,
            fixed: false,
            maxmin: true,
            area: ['900px', "600px"], //宽,高
            //	offset : [ '100px', '100px' ],// top,left
            content: basePath + '/icon.html'
        });
    });

    //删除子节点
    $(document).on("click", ".zijiedian_delete", function () {
        $(this).parent().remove();
    });
    //添加子节点
    $(document).on("click", "#zijiedian_add", function () {
        var all = '<li><input type="text" name="" class="zijiedian_name" placeholder=":name" />&nbsp;<input type="text" name="" class="zijiedian_key" placeholder=":key"/>&nbsp;<img class="butdelete zijiedian_delete"  src="' + basePath + '/istyle/images/delete.png" /></li>';
        $("#zijiedian").append(all);
    });

    //关闭当前弹出层
    $("#close").click(function () {
        var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层
    });

    form.verify({
        uniqueCode: function (value, item) {
            var url = basePath + contro_path + "/searchDetail";
            var res = "";
            web.ajax_sync(url, 'post', {systemCode: $("#systemCode").val(), code: value}, function (data) {
                if (data.result != null) {
                    res = data.result.id;
                }
            });
            if (res.length > 0) {
                return ' ' + value + '  已被注册';
            }
        }
    });
    //保存
    form.on('submit(save)', function (data) {
        if (sys_code_flag) {
            layer.alert("没有系统数据,无法查询");
            return;
        }
        var edit_data = data.field;
        //整理数据
        $(".zijiedian_name").each(function (i) {
            this.name = "list[" + i + "].name";
        });
        $(".zijiedian_key").each(function (i) {
            this.name = "list[" + i + "].key";
        });

        var edit_data = $("#menu_form").serialize();
        var url = basePath + contro_path + "/save";

        web.load();
        web.ajax(url, 'post', edit_data, function (data) {
            layer.alert("保存成功", {
                icon: 1
            }, function (index) {
                layer.close(index);//关闭当前层

                var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层

                parent.$("#query_data").click();
            });
        });
        return false;
    });


    //-------------------------------------------------------------
    // 自定义方法区
    function searpmenu(grade) {
        if (grade == "0") {
            $("#icon").val(icon1);
            $("#select_icon").html(icon1);

            $("#parentCode").html('<option value="0">主菜单</option>');
            form.render('select');
        } else {
            $("#icon").val(icon2);
            $("#select_icon").html(icon2);

            $.post(basePath + "/ssomenu/list", {
                grade: grade
                , systemCode: $("#systemCode").val()
            }, function (data) {
                var json = data.result;
                var all = "";
                for (var i = 0; i < json.length; i++) {
                    all += '<option value="' + json[i].code + '">' + json[i].name + '</option>';
                }
                $("#parentCode").html(all);
                form.render('select');

            });
        }

    }
});