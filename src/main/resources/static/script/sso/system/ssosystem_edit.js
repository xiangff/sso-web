layui.use(['element', 'form', 'layer', 'verify', 'web', 'upload'], function () {
    var element = layui.element;
    var form = layui.form;
    var layer = layui.layer;
    var web = layui.web;
    var upload = layui.upload;

    var contro_path = '/ssosystem';
    //…

    var id = $("#id").val();
    var obj = null;
    var code ;
    if (id != null && id != "") {
        var url = basePath + contro_path + "/searchDetail";
        web.load();
        web.ajax_sync(url, 'post', {id: id}, function (data) {
            obj = data.result;
            $("input[name='code']").val(obj.code);
            $("input[name='name']").val(obj.name);
            $("input[name='url']").val(obj.url);
            $("input[name='icon']").val(obj.icon);
            $("input[name='remark']").val(obj.remark);
            $("input[name='sequence']").val(obj.sequence);

            $(".uploadImg").attr("src", obj.icon);

            code = obj.code;

            form.render();
        });
    }

    var uploadInst = upload.render({
        elem: '.uploadImg'
        , url: upload_url
        , data: {projectName: upload_project_name, moduleName: 'system'}
        , accept: 'file'
        , ext: 'jpg|png'
        , done: function (res) {
            if (res.success) {
                var src = res.result.src;
                $(".uploadImg").attr("src", src);
                $("#icon").val(src);
            } else {
                layer.alert("图片上传失败! \n " + res.message);
            }
        }
        , error: function () {
            layer.alert("图片上传失败!");
        }
    });
    form.verify({
        uniqueCode: function (value, item) {
            if (value == code) {
                return;
            }
            var url = basePath + contro_path + "/searchDetail";
            var res = "";
            web.ajax_sync(url, 'post', {code: value}, function (data) {
                if (data.result != null) {
                    res = data.result.code;
                }
            });
            if (res.length > 0) {
                return ' ' + value + '  已被注册';
            }
        }
    });

    //关闭当前弹出层
    $("#close").click(function () {
        var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层
    });
    //保存
    form.on('submit(save)', function (data) {
        var url = basePath + contro_path + "/save";
        console.log(data.field);
        web.ajax(url, 'post', data.field, function (data) {
            layer.alert("保存成功", {
                icon: 1
            }, function (index) {
                layer.close(index);//关闭当前层

                var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层



                parent.$("#query_data").click();
            });
        });
        return false;
    });

});