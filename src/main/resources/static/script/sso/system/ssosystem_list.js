layui.use(['element', 'form', 'layer', 'table', 'laypage', 'web', 'user'], function () {
    var element = layui.element;
    var form = layui.form;
    var layer = layui.layer;
    var table = layui.table;
    var web = layui.web;
    var user = layui.user;

    var table_list_id = 'table_list_id'; // 表格id
    var table_list_tool = "table_list_tool"; //表格工具
    var contro_path = '/ssosystem'; //controller 的路径

    //查询数据
    $("#query_data").click(function () {
        web.load();
        web.page_query("#", "#", function (list) {
            table.render({
                elem: '#' + table_list_id
                , skin: 'line,row' //行,列边框
                , even: true //开启隔行背景
                , height: table_height
                , data: list
                , cols: [
                    [
                        {type: 'checkbox'}
                        , {field: 'index', title: '序号', width: 70, templet: '<div>{{ d.LAY_INDEX }}</div>'}
                        , {field: 'code', title: '系统编号'}
                        , {field: 'name', title: '系统名称'}
                        , {field: 'url', title: '系统链接',width:200}
                        , {field: 'icon', title: '图标', templet: tpl_img, width: 80}
                        , {field: 'remark', title: '备注'}
                        , {field: 'sequence', title: '顺序'}
                        , {field: 'createTime', title: '创建时间', templet: tpl_createTime, width: 160}
                        , {field: 'createUser', title: '创建人'}
                        , {field: 'updateTime', title: '修改时间', templet: tpl_updateTime, width: 160}
                        , {field: 'updateUser', title: '修改人'}
                        , {fixed: 'right', title: '操作', width: 150, align: 'left', toolbar: '#list_tool_tpl'}
                    ]
                ]
            });
            user.checkAuth();
            layer.photos({
                photos: '.layer-photos-demo'
                , anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
            });
        });
    });

    function tpl_img(d) {
        var html = '<div class="layer-photos-demo">' +
            '<img width="35px;" height="35px;"   layer-pid="' + d.id + '" layer-src="' + d.icon + '" src="' + d.icon + '" alt="' + d.name + '" />' +
            '</div>';
        return html;
    }


    //监听工具条==>操作
    table.on('tool(' + table_list_tool + ')', function (obj) {
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的DOM对象
        //查看
        if (layEvent === 'detail') {
            //do somehing
        }
        //删除
        else if (layEvent === 'del') {
            web.delete_select(contro_path, data.id, function () {
                obj.del();
            });
        }
        //编辑
        else if (layEvent === 'edit') {
            show_edit(data.id);
        }

    });
    /**
     * '全部删除' 按钮
     */
    $("#delete_all").click(function () {
        web.delete_select_all(contro_path, table_list_id, function () {
            $("#query_data").click();
        });
    });
    /*----------------------------------------------------------
            弹出层
    ------------------------------------------------------------ */
    var ifame_height = '650px'; //弹出层的高度
    var title_name = '系统信息';


    //添加
    $("#add").click(function () {
        var index_add = layer.open({
            title: ['新增' + title_name, font_set]
            , type: 2
            , fixed: false
            , maxmin: true
            , area: ['600px', ifame_height]     //宽,高
            , offset: ['100px', '100px']    // top,left
            , content: web.url_replace('sso/system/ssosystem_add')
        });
    });

    //编辑
    function show_edit(id) {
        var index_edit = layer.open({
            title: ['编辑' + title_name, font_set]
            , type: 2
            , fixed: false
            , maxmin: true
            , area: ['600px', ifame_height] //宽,高
            , offset: ['100px', '100px']// top,left
            , content: web.url_replace('sso/system/ssosystem_edit?id=' + id)
        });
    }

});