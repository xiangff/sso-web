layui.use(['element', 'form', 'layer', 'verify', 'web', 'upload'], function () {
    var element = layui.element;
    var form = layui.form;
    var layer = layui.layer;
    var web = layui.web;
    var upload = layui.upload;

    var contro_path = '/ssosystem';
    //…
    $(".layui-input")[0].focus();

    var uploadInst = upload.render({
        elem: '.uploadImg'
        , url: upload_url
        , data: {projectName: upload_project_name, moduleName: 'system'}
        , accept: 'file'
        , ext: 'jpg|png'
        , done: function (res) {
            if (res.success) {
                var src = res.result.src;
                $(".uploadImg").attr("src", src);
                $("#icon").val(src);
            } else {
                layer.alert("图片上传失败! \n " + res.message);
            }
        }
        , error: function () {
            layer.alert("图片上传失败!");
        }
    });

    //关闭当前弹出层
    $("#close").click(function () {
        var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层
    });
    form.verify({
        uniqueCode: function (value, item) {
            var url = basePath + contro_path + "/searchDetail";
            var res = "";
            web.ajax_sync(url, 'post', {code: value}, function (data) {
                if (data.result != null) {
                    res = data.result.code;
                }
            });
            if (res.length > 0) {
                return ' ' + value + '  已被注册';
            }
        }
    });
    //保存
    form.on('submit(save)', function (data) {
        var url = basePath + contro_path + "/save";
        var edit_data = data.field;
        delete edit_data.file;
        //console.log(data.field);
        web.load();
        web.ajax(url, 'post', edit_data, function (data) {
            layer.alert("保存成功", {
                icon: 1
            }, function (index) {
                layer.close(index);//关闭当前层

                var current_index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                parent.layer.close(current_index); //用父窗口的layer对象关闭当前弹出层

                parent.$("#query_data").click();
            });
        });
        return false;
    });

});
