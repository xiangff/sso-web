/*暂时没有启用*/
/*html界面初始
<script src="https://code.jquery.com/jquery-3.1.1.min.js" type="text/javascript" charset="utf-8"></script>
<script th:src="@{{path}/istyle/js/html.init.js(path=${application.basePath}) }" type="text/javascript" charset="utf-8"></script>
*/
/**
 * 界面公用地方的头部
 */
function init_header() {
    var basePath = "/sso";
    var resource = "http://www.sesame.kim/zy";

    $.include(basePath+"/istyle/js/common.js");
    $.include(basePath+"/istyle/js/data.js");
    // layui
    $.include(resource+"/istyle/libs/layui-last/layui/layui.js");
    $.include(resource+"/istyle/libs/layui-last/layui/css/layui.css");

    $.include(basePath+"/istyle/css/style.css");

    window.basePath = basePath;
    window.defaultPageSize = 10;

    layui.extend({
        web: '{/}'+basePath+'/istyle/layui_module/web'
        ,user: '{/}'+basePath+'/istyle/layui_module/user'
    })
}

/**
 * 界面公用地方的尾部
 */
function init_footer() {

}

/**
 * 加载js 和css
 */
$.extend({
    includePath: '',
    include: function(file) {
        var files = typeof file == "string" ? [file]:file;
        for (var i = 0; i < files.length; i++) {
            var name = files[i].replace(/^\s|\s$/g, "");
            var att = name.split('.');
            var ext = att[att.length - 1].toLowerCase();
            var isCSS = ext == "css";
            var tag = isCSS ? "link" : "script";
            var attr = isCSS ? " type='text/css' rel='stylesheet' " : " language='javascript' type='text/javascript' ";
            var link = (isCSS ? "href" : "src") + "='" + $.includePath + name + "'";
            if ($(tag + "[" + link + "]").length == 0) document.write("<" + tag + attr + link + "></" + tag + ">");
        }
    }
});
