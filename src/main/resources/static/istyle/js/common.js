﻿/*------------------------------------------------
    table templet 的公用方法
------------------------------------------------*/
/**
 * 创建时间
 */
function tpl_createTime(d) {
    return toDateString(d.createTime);
}

/**
 * 修改时间
 */
function tpl_updateTime(d) {
    return toDateString(d.updateTime);
}

/**
 * 判断性别
 * @param d
 * @returns {*}
 */
function tpl_sex(d) {
    if (d.sex == '1') {
        return '男'
    } else if (d.sex == '2') {
        return '女';
    } else {
        return '未知';
    }
}

/**
 * 格式化时间
 */
function toDateString(d, format) {
    var date = new Date(d || new Date())
        , ymd = [
        this.digit(date.getFullYear(), 4)
        , this.digit(date.getMonth() + 1)
        , this.digit(date.getDate())
    ]
        , hms = [
        this.digit(date.getHours())
        , this.digit(date.getMinutes())
        , this.digit(date.getSeconds())
    ];

    format = format || 'yyyy-MM-dd HH:mm:ss';

    return format.replace(/yyyy/g, ymd[0])
        .replace(/MM/g, ymd[1])
        .replace(/dd/g, ymd[2])
        .replace(/HH/g, hms[0])
        .replace(/mm/g, hms[1])
        .replace(/ss/g, hms[2]);
}

/**
 * 数字前置补零
 */
function digit(num, length, end) {
    var str = '';
    num = String(num);
    length = length || 2;
    for (var i = num.length; i < length; i++) {
        str += '0';
    }
    return num < Math.pow(10, length) ? str + (num | 0) : num;
};

/*-----------------------------------------------------------------------------------------------------*/
/*--------------------------     js 对url 的操作         ------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------*/
function getRootPath_web() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPaht = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPaht + projectName);
}
function getUrlParam(k) {
    var regExp = new RegExp('([?]|&)' + k + '=([^&]*)(&|$)');
    var result = window.location.href.match(regExp);
    if (result) {
        return decodeURIComponent(result[2]);
    } else {
        return null;
    }
}