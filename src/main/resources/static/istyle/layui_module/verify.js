/**
 * from 表单的公共校验
 *
 * @author johnny
 * @date 2017/12/7 11:07
 */
layui.define(['form'], function (exports) {
    var form = layui.form;
    var $ = layui.jquery;

    var verify = form.verify({
        /** 字符编号,校验, 比如说用户编号, */
        charCode: function (value, item) {
            if (!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)) {
                return '不能为空和有特殊字符';
            }
            if (/(^\_)|(\__)|(\_+$)/.test(value)) {
                return '首尾不能出现下划线\'_\'';
            }
            if (/^\d+\d+\d$/.test(value)) {
                return '不能全为数字';
            }
        }
        /** 手机号码校验 , 允许为空 */
        , aePhone: function (value, item) {
            if (value == null || value.length == 0) {
                return;
            } else if (!new RegExp("^1\\d{10}$").test(value)) {
                return "手机号码格式错误 , 该项允许为空";
            }
        }
        /** 邮箱校验 , 允许为空 */
        , aeEmail: function (value, item) {
            if (value == null || value.length == 0) {
                return;
            } else if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value) == false) {
                return "邮箱地址格式错误 , 该项允许为空";
            }
        }
        /** 链接格式校验 , 允许为空 */
        , aeUrl: function (value, item) {
            if (value == null || value.length == 0) {
                return;
            } else if (/(^#)|(^http(s*):\/\/[^\s]+\.[^\s]+)/.test(value)==false) {
                return "链接格式错误 , 该项允许为空";
            }
        }
        /** 身份证号校验 , 允许为空 */
        , aeIdentity: function (value, item) {
            if (value == null || value.length == 0) {
                return;
            } else if (/(^\d{15}$)|(^\d{17}(x|X|\d)$)/.test(value) == false) {
                return "身份证号格式错误 , 该项允许为空";
            }
        }
        /** 数字校验 , 允许为空 */
        , aeNumber: function (value, item) {
            if (value == null || value.length == 0) {
                return;
            } else if (!value || isNaN(value)) {
                return "数字格式错误 , 该项允许为空";
            }
        }

        , pwd: [/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9a-zA-Z]{8,16}$/, "请输入8-16位数字和字母的组合"]
        , comfirmPwd: function (value, item) {
            var password = $(item).parent().parent().prev().find("input").eq(0).val();
            if (value != password) {
                return "两次密码不一样";
            }
        }
        /**比例 (0-100的整数)*/
        , ratio: [/^([0-9]{1,2}|100)$/, "请输入0-100的整数"]
        , money: [/^(([0-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/, '请输入正确的金额']
    });


    exports('verify', {verify: verify});
});
