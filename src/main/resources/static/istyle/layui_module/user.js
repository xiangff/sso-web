/**
 *用户相关信息
 * @author johnny
 * @date 2017/12/7 11:07
 */
layui.define(['layer', 'web'], function (exports) {
    var layer = layui.layer;
    var web = layui.web;
    var $ = layui.jquery;
    var user_table = "user_table";

    var obj = {
        getUser: function () {
            var session_user = layui.sessionData(user_table).user;
            if (session_user == null) {
                initUserData();
                session_user = layui.sessionData(user_table).user;
            }
            return session_user;
        }
        , getRole: function () {
            var session_role = layui.sessionData(user_table).roles;
            if (session_role == null) {
                initUserData();
                session_role = layui.sessionData(user_table).roles;
            }
            return session_role;
        }
        , initData: function () {
            return initUserData();
        }
        /**
         * 清空用户数据
         */
        , cleanUser: function () {
            layui.sessionData(user_table, {key: 'user', remove: true});
            layui.sessionData(user_table, {key: 'roles', remove: true});
        }
        /**
         * 检测权限,控制权限标签的显示
         */
        , checkAuth: function () {
            var auth_tag = $("*[authkey]");
            if (auth_tag.length > 0) {
                var arr = getAuth();
                auth_tag.each(function () {
                    var key = $(this).attr("authkey");
                    if (contains(arr, key)) {
                        //console.log("有权限: " + key);
                        $(this).show();
                    } else {
                        //console.log("没有权限: " + key);
                        $(this).remove();
                    }
                });

            }// if
        }
        /**
         * 清空权限
         */
        , cleanAuth: function () {
            layui.sessionData(user_table, {key: 'auths', remove: true});
        }
    };

    function initUserData() {
        var userInfo, roleInfo;
        var flag = false;
        var url = basePath + "/user/getUserInfo";
        web.ajax_sync(url, 'post', {}, function (data) {
            userInfo = data.result.user;
            roleInfo = data.result.roles;

            layui.sessionData(user_table, {key: 'user', value: userInfo});
            layui.sessionData(user_table, {key: 'roles', value: roleInfo});
            flag = true;
        });
        return flag;
    }

    function getAuth() {
        var auths = layui.sessionData(user_table).auths;
        if (auths == null) {
            var url = basePath + "/user/getAuthKeys";
            web.ajax_sync(url, 'post', {}, function (data) {
                auths = data.result;
                layui.sessionData(user_table, {key: 'auths', value: auths});
            });
        }
        return auths;
    }

    function contains(arr, obj) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == obj) {
                return true;
            }
        }
        return false;
    }

    exports('user', obj);
});
