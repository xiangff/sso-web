package com.sesame;

import kim.sesame.framework.web.context.SpringContextUtil;
import kim.sesame.framework.web.init.WebInit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;


/**
 * @author johnny
 * @date 2018-01-25 12:41
 * @Description:@ServletComponentScan : 监听器
 */
//@EnableFeignClients
//@EnableDiscoveryClient
@ServletComponentScan(basePackageClasses = WebInit.class)
@SpringBootApplication
public class AppSSO {

    public static void main(String[] arr) {

        SpringApplication.run(AppSSO.class, arr);
        SpringContextUtil.printInfo();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println(" 我停止了...");
        }));


    }
}