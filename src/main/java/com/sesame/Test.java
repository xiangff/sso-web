package com.sesame;

import com.github.kevinsawicki.http.HttpRequest;
import kim.sesame.framework.utils.GData;

import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYmYiOjE1MjY1NTgxNjIsImp3dF9zZXNzaW9uX2lkIjoiMDA5MTUzNDQ2OEY3MDZGMjlFNEI3MTlBOUUwRkM3NDciLCJpc3MiOiIwMDAwIiwiand0X2NyZWF0ZV90aW1lIjoxNTI2NTU4MTYyMzU3LCJleHAiOjE1MjcxNjI5NjJ9.6c0eqqoLmT7m3HfemA5OUKBWKsaAxIRHRm-3NZODXIQ";

        String url = "http://127.0.0.1:8090/sso/jwt/test";
        Map<String, String> headers = new HashMap<>();
        headers.put(GData.JWT.TOKEN, token);
        String res = HttpRequest.post(url).headers(headers).body();
        System.out.println(res);
    }
}
