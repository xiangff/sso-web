package com.sesame.basic.controller;

import com.sesame.basic.ProjectConfig;
import com.sesame.sso.menu.service.SsoMenuService;
import com.sesame.sso.system.bean.SsoSystem;
import com.sesame.sso.system.service.SsoSystemService;
import com.sesame.sso.user.bean.SsoUser;
import kim.sesame.framework.entity.GMap;
import kim.sesame.framework.web.annotation.IgnoreLoginCheck;
import kim.sesame.framework.web.context.UserContext;
import kim.sesame.framework.web.controller.AbstractWebController;
import kim.sesame.framework.web.entity.IUser;
import kim.sesame.framework.web.response.Response;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


@CommonsLog
@Controller
@RequestMapping("/index")
public class IndexController extends AbstractWebController {

    @Autowired
    private SsoMenuService ssoMenuService;
    @Autowired
    private SsoSystemService ssoSystemService;

    @IgnoreLoginCheck
    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @IgnoreLoginCheck(isLoadUser = true)
    @RequestMapping("/index")
    public String index(ModelMap modelMap) {
        IUser user = UserContext.getUserContext().getUser();
        modelMap.put("isLogin", user != null);

        return "index";
    }

    @RequestMapping("/getSysmenu")
    @ResponseBody
    public List<GMap> getSysmenu(HttpServletRequest request) {

        SsoUser user = UserContext.getUserContext().getUser(SsoUser.class);
        ServletContext application = request.getServletContext();
        String basePath = application.getAttribute("basePath").toString();

        return ssoMenuService.queryUserMenu(user.getAccount(), ProjectConfig.SysCode, basePath);
    }

    @RequestMapping("/system")
    @ResponseBody
    public Response system() {
        IUser user = UserContext.getUserContext().getUser();

        List<SsoSystem> list = ssoSystemService.querySystemByUser(user.getAccount());

        return returnSuccess(list);
    }
}