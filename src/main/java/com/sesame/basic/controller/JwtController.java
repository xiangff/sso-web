package com.sesame.basic.controller;

import kim.sesame.framework.utils.GData;
import kim.sesame.framework.web.context.UserContext;
import kim.sesame.framework.web.controller.AbstractWebController;
import kim.sesame.framework.web.entity.IUser;
import kim.sesame.framework.web.jwt.JwtHelper;
import kim.sesame.framework.web.response.Response;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@CommonsLog
@RestController
@RequestMapping("/jwt")
public class JwtController extends AbstractWebController {

    @RequestMapping("/getToken")
    public Response getToken() {

        Map par = new HashMap();
        par.put(GData.JWT.SESSION_ID, UserContext.getUserContext().getUserSessionId());

        String token = JwtHelper.createJWT(par);
        return returnSuccess(token);
    }

    @RequestMapping("/test")
    public Response test() {
        IUser user = UserContext.getUserContext().getUser();

        return returnSuccess(user);
    }
}
