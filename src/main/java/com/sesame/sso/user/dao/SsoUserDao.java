package com.sesame.sso.user.dao;

import com.sesame.sso.user.bean.SsoUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * SsoUserDao
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 用户表
 */
 @Mapper
public interface SsoUserDao {

	/** 新增  */
	int insert(SsoUser bean);
	
	/** 删除  */
	int delete(String id);
	
	/** 修改  */
	int update(SsoUser bean);
	
	/** 查询list */
	List<SsoUser> selectList(SsoUser bean);
	
	/** 查询单个,返回bean */
	SsoUser search(SsoUser bean);

}
