package com.sesame.sso.user.service;

import com.sesame.sso.user.bean.SsoUser;
import com.sesame.sso.user.dao.SsoUserDao;
import kim.sesame.framework.mybatis.dao.BaseDao;
import kim.sesame.framework.utils.GData;
import kim.sesame.framework.utils.MD5Util;
import kim.sesame.framework.utils.StringUtil;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * SsoUserService
 *
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 用户表
 */
@CommonsLog
@Service
public class SsoUserService {

    @SuppressWarnings("all")
    @Resource
    private SsoUserDao ssoUserDao;
    @SuppressWarnings("all")
    @Autowired
    BaseDao baseDao;

    /**
     * 查询list
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description: 分页时要注意
     */
    public List<SsoUser> searchList(SsoUser bean) {

        bean.setActive(GData.BOOLEAN.YES);

        List<SsoUser> list = ssoUserDao.selectList(bean);

        return list;
    }

    /**
     * 新增
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int add(SsoUser bean) {
        bean.setActive(GData.BOOLEAN.YES);
        bean.setIsDelete(GData.BOOLEAN.YES);
        if(StringUtil.isNotEmpty(bean.getPwd())){
            bean.setPwd(MD5Util.encodeByMD5(bean.getPwd()));
        }
        int res = ssoUserDao.insert(bean);

        return res;
    }

    /**
     * 修改
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int update(SsoUser bean) {

        int res = ssoUserDao.update(bean);

        return res;
    }

    /**
     * 删除
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int delete(String ids) {

        List<String> list_id = Stream.of(ids.split(",")).map(String::trim).distinct().filter(StringUtil::isNotEmpty).collect(Collectors.toList());
        list_id.stream().forEach(this::deleteById);

        return list_id.size();
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteById(String id) {
        //物理删除
        ssoUserDao.delete(id);

        //逻辑删除
//    SsoUser bean = new SsoUser();
//    	bean.setId(id);
//    	bean.setActive(GData.BOOLEAN.NO);
//    	bean.setUpdateTime(new Date());
//    	update(bean);
    }

    /**
     * 查询--返回bean
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    public SsoUser search(SsoUser bean) {
        return ssoUserDao.search(bean);
    }


}
