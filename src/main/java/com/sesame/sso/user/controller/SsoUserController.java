package com.sesame.sso.user.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sesame.sso.cache.SsoCacheServer;
import com.sesame.sso.user.bean.SsoUser;
import com.sesame.sso.user.service.SsoUserService;
import kim.sesame.framework.entity.GPage;
import kim.sesame.framework.mybatis.uitl.PageUtil;
import kim.sesame.framework.utils.StringUtil;
import kim.sesame.framework.web.context.UserContext;
import kim.sesame.framework.web.controller.AbstractWebController;
import kim.sesame.framework.web.entity.IUser;
import kim.sesame.framework.web.response.Response;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * SsoUserController
 *
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 用户表
 */
@CommonsLog
@RestController
@RequestMapping("/ssouser")
public class SsoUserController extends AbstractWebController {

    @Resource
    private SsoUserService ssoUserService;

    /**
     * 查询list
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping("/list")
    public Response list(SsoUser bean) {

        List<SsoUser> list = ssoUserService.searchList(bean);

        return returnSuccess(list);
    }

    /**
     * 分页查询
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @ pageNum : 页码,默认1
     * @ pageSize :  页面大小
     */
    @RequestMapping("/listPage")
    public Response listPage(SsoUser bean, GPage gPage) {

        Page<SsoUser> pages = PageHelper.startPage(gPage.getPageNum(), gPage.getPageSize())
                .doSelectPage(() -> ssoUserService.searchList(bean));
        List<SsoUser> list = pages.getResult();

        return returnSuccess(list, PageUtil.recount(gPage, pages));
    }

    /**
     * 保存
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping(value = "/save", method = {RequestMethod.POST})
    public Response save(SsoUser bean) {

        IUser user = UserContext.getUserContext().getUser();
        int res = 0;
        if (StringUtil.isNotEmpty(bean.getId())) {
            // 主键不为空, 修改数据
            bean.initUpdate(user.getAccount());
            res = ssoUserService.update(bean);
        } else {
            //主键为空, 新增数据
            bean.initCreateAndId(user.getAccount());
            res = ssoUserService.add(bean);
        }
        return returnSuccess(res);
    }

    /**
     * 删除
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping("/delete")
    public Response delete(String ids, HttpServletRequest request) {
        int res = ssoUserService.delete(ids);
        return returnSuccess(res);
    }

    /**
     * 查询详情
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping("/searchDetail")
    public Response searchDetail(SsoUser bean, HttpServletRequest request) {
        bean = ssoUserService.search(bean);
        return returnSuccess(bean);
    }

    @Autowired
    SsoCacheServer ssoCacheServer;

    @RequestMapping("/cleanUserCache")
    public Response cleanUserCache(String account) {

        ssoCacheServer.queryUserByAccount_invalid(account);

        return returnSuccess();
    }

}
