package com.sesame.sso.user.bean;

import kim.sesame.framework.utils.GData;
import kim.sesame.framework.utils.UUIDUtil;
import kim.sesame.framework.web.entity.IUser;
import lombok.Data;

import java.util.Date;

/**
 * SsoUser
 *
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 用户表
 */
@Data
public class SsoUser extends IUser {
    private static final long serialVersionUID = 1L;


    public SsoUser() {
    }

    public SsoUser(String account, String roleCode, String name, String pwd) {
        super.setAccount(account);
        super.setName(name);
        super.setPwd(pwd);

        setId(UUIDUtil.getShortUUID());

        Date d = new Date();
        setCreateTime(d);
        setCreateUser(GData.SYS.ADMIN_USER);
        setUpdateTime(d);
        setUpdateUser(GData.SYS.ADMIN_USER);

        super.setRoleCode(roleCode);
        super.setIsDelete(GData.BOOLEAN.NO);
        super.setIsEnable(GData.BOOLEAN.YES);
        super.setActive(GData.BOOLEAN.YES);
    }
}