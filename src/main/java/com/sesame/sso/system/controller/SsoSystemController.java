package com.sesame.sso.system.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sesame.sso.system.bean.SsoSystem;
import com.sesame.sso.system.service.SsoSystemService;
import kim.sesame.framework.entity.GPage;
import kim.sesame.framework.entity.ZTree;
import kim.sesame.framework.mybatis.uitl.PageUtil;
import kim.sesame.framework.utils.Argument;
import kim.sesame.framework.utils.StringUtil;
import kim.sesame.framework.web.context.UserContext;
import kim.sesame.framework.web.controller.AbstractWebController;
import kim.sesame.framework.web.entity.IUser;
import kim.sesame.framework.web.response.Response;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * SsoSystemController
 *
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 系统表
 */
@CommonsLog
@RestController
@RequestMapping("/ssosystem")
public class SsoSystemController extends AbstractWebController {

    @Resource
    private SsoSystemService ssoSystemService;

    /**
     * 查询list
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping("/list")
    public Response list(SsoSystem bean) {

        List<SsoSystem> list = ssoSystemService.searchList(bean);

        return returnSuccess(list);
    }

    /**
     * 分页查询
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @ pageNum : 页码,默认1
     * @ pageSize :  页面大小
     */
    @RequestMapping("/listPage")
    public Response listPage(SsoSystem bean, GPage gPage) {

        Page<SsoSystem> pages = PageHelper.startPage(gPage.getPageNum(), gPage.getPageSize())
                .doSelectPage(() -> ssoSystemService.searchList(bean));
        List<SsoSystem> list = pages.getResult();

        return returnSuccess(list, PageUtil.recount(gPage, pages));
    }

    /**
     * 保存
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping(value = "/save", method = {RequestMethod.POST})
    public Response save(SsoSystem bean) {
        IUser user = UserContext.getUserContext().getUser();
        int res = 0;
        if (StringUtil.isNotEmpty(bean.getId())) {
            // 主键不为空, 修改数据
            bean.initUpdate(user.getAccount());
            res = ssoSystemService.update(bean);
        } else {
            //主键为空, 新增数据
            bean.initCreateAndId(user.getAccount());
            res = ssoSystemService.add(bean);
        }
        return returnSuccess(res);
    }

    /**
     * 删除
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping("/delete")
    public Response delete(String ids, HttpServletRequest request) {
        int res = ssoSystemService.delete(ids);
        return returnSuccess(res);
    }

    /**
     * 查询详情
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping("/searchDetail")
    public Response searchDetail(SsoSystem bean, HttpServletRequest request) {
        bean = ssoSystemService.search(bean);
        return returnSuccess(bean);
    }

    /**
     * 获取系统树结构数据
     *
     * @param roleCode   角色编号
     * @return
     */
    @RequestMapping("/findSystemTreeByCode")
    public Response findSystemTreeByCode(String roleCode) {
        Argument.notEmpty(roleCode, "角色编号 code 不能为空");

        List<ZTree> list = ssoSystemService.searchRoleSystem(roleCode);

        return returnSuccess(list);
    }

    /**
     * 保存节点数据
     *
     * @param roleCode   角色编号
     * @param systemCodes  系统节点集合,  逗号分隔
     * @return
     */
    @RequestMapping("/saveMenuTreeData")
    public Response saveMenuTreeData(String roleCode, String systemCodes) {
        Argument.notEmpty(roleCode, "角色编号 code 不能为空");

        ssoSystemService.saveMenuTreeData(roleCode, systemCodes);

        return returnSuccess();
    }

}
