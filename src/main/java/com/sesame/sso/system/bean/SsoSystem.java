package com.sesame.sso.system.bean;

import kim.sesame.framework.entity.BaseEntity;
import lombok.Data;

/**
 * SsoSystem
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 系统表
 */
@Data
public class SsoSystem extends BaseEntity {
	private static final long serialVersionUID = 1L;

	private String code;//系统编号 唯一
	private String name;//系统名称
	private String url;//系统链接
	private String icon;//系统图标
	private String remark;//备注
	private Integer sequence;//顺序(根据级别)
	private String active;//是否有效 : Y/N

    // not database field ...
	
}