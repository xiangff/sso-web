package com.sesame.sso.system.dao;

import com.sesame.sso.system.bean.SsoSystem;
import kim.sesame.framework.entity.GMap;
import kim.sesame.framework.entity.ZTree;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * SsoSystemDao
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 系统表
 */
 @Mapper
public interface SsoSystemDao {

	/** 新增  */
	int insert(SsoSystem bean);
	
	/** 删除  */
	int delete(String id);
	
	/** 修改  */
	int update(SsoSystem bean);
	
	/** 查询list */
	List<SsoSystem> selectList(SsoSystem bean);
	
	/** 查询单个,返回bean */
	SsoSystem search(SsoSystem bean);


    List<ZTree> searchRoleSystem(GMap params);

	int cleanRoleSystem(GMap params);

	int saveRoleSystem(GMap params);


    List<SsoSystem> querySystemByUser(String account);
}
