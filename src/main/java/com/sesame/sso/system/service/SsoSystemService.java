package com.sesame.sso.system.service;

import com.sesame.sso.role.service.SsoRoleService;
import com.sesame.sso.system.bean.SsoSystem;
import com.sesame.sso.system.dao.SsoSystemDao;
import kim.sesame.framework.entity.GMap;
import kim.sesame.framework.entity.ZTree;
import kim.sesame.framework.utils.GData;
import kim.sesame.framework.utils.StringUtil;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * SsoSystemService
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 系统表
 */
@CommonsLog
@Service
public class SsoSystemService {

    @SuppressWarnings("all")
	@Resource
	private SsoSystemDao ssoSystemDao;
	@Autowired
	private SsoRoleService ssoRoleService;

	/**
	 * 查询list
	 * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description: 分页时要注意
	 */
	public List<SsoSystem> searchList(SsoSystem bean) {
		bean.setActive(GData.BOOLEAN.YES);
		
		List<SsoSystem> list = ssoSystemDao.selectList(bean);

		return list;
	}
	
	/**
	 * 新增
	 * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:	
	 */
	@Transactional(rollbackFor = Exception.class)
	public int add(SsoSystem bean)  {

		bean.setActive(GData.BOOLEAN.YES);
		int res = ssoSystemDao.insert(bean);
		
		return res;
	}
	
	/**
	 * 修改
	 * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:	
	 */
	@Transactional(rollbackFor = Exception.class)
	public int update(SsoSystem bean)  {

		int res = ssoSystemDao.update(bean);
		
		return res;
	}
	
	/**
	 * 删除
	 * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:	
	 */
	@Transactional(rollbackFor = Exception.class)
    public int delete(String ids) {

        List<String> list_id = Stream.of(ids.split(",")).map(String::trim).distinct().filter(StringUtil::isNotEmpty).collect(Collectors.toList());
        list_id.stream().forEach(this::deleteById);

        return list_id.size();
    }
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(String id) {
         //物理删除
      //  ssoSystemDao.delete(id);

        //逻辑删除
		SsoSystem bean = new SsoSystem();
		bean.setId(id);
		bean.setActive(GData.BOOLEAN.NO);
		bean.setUpdateTime(new Date());
		update(bean);
	}
	
	/**
	 * 查询--返回bean
	 * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:	
	 */
	public SsoSystem search(SsoSystem bean)  {

		return ssoSystemDao.search(bean);
	}

    public List<ZTree> searchRoleSystem(String roleCode) {
		GMap params = GMap.newMap();
		params.putAction("roleCode", roleCode);
		params.putAction("userRoleCodes", ssoRoleService.getUserRoleCodes());

		List<ZTree> list = ssoSystemDao.searchRoleSystem(params);
		return list;
    }

	@Transactional(rollbackFor = Exception.class)
	public void saveMenuTreeData(String roleCode, String systemCodes) {
		GMap params = GMap.newMap();
		params.putAction("roleCode", roleCode);

		// 1 先清除原来的数据
		ssoSystemDao.cleanRoleSystem(params);

		//2 保存数据
		List<String> codes = Arrays.asList(systemCodes.split(","));
		if (codes != null && codes.size() > 0) {
			params.putAction("codes", codes);

			ssoSystemDao.saveRoleSystem(params);
		}
	}

    public List<SsoSystem> querySystemByUser(String account) {
		return  ssoSystemDao.querySystemByUser(account);
    }
}
