package com.sesame.sso.dic.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sesame.sso.dic.bean.SsoDataDic;
import com.sesame.sso.dic.service.SsoDataDicService;
import kim.sesame.framework.entity.GPage;
import kim.sesame.framework.mybatis.uitl.PageUtil;
import kim.sesame.framework.utils.StringUtil;
import kim.sesame.framework.web.controller.AbstractWebController;
import kim.sesame.framework.web.response.Response;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * SsoDataDicController
 * @author johnny
 * @date 2018-03-09 17:28:10
 * @Description: 系统数据字典表
 */
@CommonsLog
@RestController
@RequestMapping("/ssodatadic")
public class SsoDataDicController extends AbstractWebController {

	@Resource
	private SsoDataDicService ssoDataDicService;

	/**
	 * 查询list
	  * @author johnny
      * @date 2018-03-09 17:28:10
      * @Description:
	 */
	@RequestMapping("/list")
	public Response list(SsoDataDic bean) {

		List<SsoDataDic> list = ssoDataDicService.searchList(bean);

		return returnSuccess(list);
	}
	
	/**
	 * 分页查询
	 * @author johnny
     * @date 2018-03-09 17:28:10
     * @ pageNum : 页码,默认1
     * @ pageSize :  页面大小
	 */
	@RequestMapping("/listPage")
	public Response listPage(SsoDataDic bean, GPage gPage) {

		Page<SsoDataDic> pages = PageHelper.startPage(gPage.getPageNum(), gPage.getPageSize())
                        .doSelectPage(() -> ssoDataDicService.searchList(bean));
        List<SsoDataDic> list = pages.getResult();
		
		return returnSuccess(list, PageUtil.recount(gPage, pages));
	}
	
	/**
	 * 保存
	 * @author johnny
     * @date 2018-03-09 17:28:10
     * @Description:	
	 */
	@RequestMapping(value = "/save",method = {RequestMethod.POST})
	public Response save(SsoDataDic bean) {
		//IUser user = UserContext.getUserContext().getUser();
		int res = 0;
        if (StringUtil.isNotEmpty(bean.getId())) {
            // 主键不为空, 修改数据
            bean.initUpdate("");
            res = ssoDataDicService.update(bean);
        } else {
            //主键为空, 新增数据
            bean.initCreateAndId("");
            res = ssoDataDicService.add(bean);
        }
		return returnSuccess(res);
	}
		
	/**
	 * 删除
	 * @author johnny
     * @date 2018-03-09 17:28:10
     * @Description:	
	 */
	@RequestMapping("/delete")
	public Response delete(String ids,HttpServletRequest request) {
		int res = ssoDataDicService.delete(ids);
		return returnSuccess(res);
	}
		
	/**
	 * 查询详情
	 * @author johnny
     * @date 2018-03-09 17:28:10
     * @Description:	
	 */
	@RequestMapping("/searchDetail")
	public Response searchDetail(SsoDataDic bean,HttpServletRequest request) {
		bean = ssoDataDicService.search(bean);
		return returnSuccess(bean);
	}
		
}
