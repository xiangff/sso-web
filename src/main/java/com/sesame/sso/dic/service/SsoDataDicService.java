package com.sesame.sso.dic.service;

import com.sesame.sso.dic.bean.SsoDataDic;
import com.sesame.sso.dic.dao.SsoDataDicDao;
import kim.sesame.framework.utils.GData;
import kim.sesame.framework.utils.StringUtil;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * SsoDataDicService
 * @author johnny
 * @date 2018-03-09 17:28:10
 * @Description: 系统数据字典表
 */
@CommonsLog
@Service
public class SsoDataDicService {

    @SuppressWarnings("all")
	@Resource
	private SsoDataDicDao ssoDataDicDao;

	/**
	 * 查询list
	 * @author johnny
     * @date 2018-03-09 17:28:10
     * @Description: 分页时要注意
	 */
	public List<SsoDataDic> searchList(SsoDataDic bean) {

    	bean.setActive(GData.BOOLEAN.YES);
		
		List<SsoDataDic> list = ssoDataDicDao.selectList(bean);

		return list;
	}
	
	/**
	 * 新增
	 * @author johnny
     * @date 2018-03-09 17:28:10
     * @Description:	
	 */
	@Transactional(rollbackFor = Exception.class)
	public int add(SsoDataDic bean)  {

	    bean.setActive(GData.BOOLEAN.YES);

		int res = ssoDataDicDao.insert(bean);
		
		return res;
	}
	
	/**
	 * 修改
	 * @author johnny
     * @date 2018-03-09 17:28:10
     * @Description:	
	 */
	@Transactional(rollbackFor = Exception.class)
	public int update(SsoDataDic bean)  {

		int res = ssoDataDicDao.update(bean);
		
		return res;
	}
	
	/**
	 * 删除
	 * @author johnny
     * @date 2018-03-09 17:28:10
     * @Description:	
	 */
	@Transactional(rollbackFor = Exception.class)
    public int delete(String ids) {

        List<String> list_id = Stream.of(ids.split(",")).map(String::trim).distinct().filter(StringUtil::isNotEmpty).collect(Collectors.toList());
        list_id.stream().forEach(this::deleteById);

        return list_id.size();
    }
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(String id) {
         //物理删除
        ssoDataDicDao.delete(id);

        //逻辑删除
//    SsoDataDic bean = new SsoDataDic();
//    	bean.setId(id);
//    	bean.setActive(GData.BOOLEAN.NO);
//    	bean.setUpdateTime(new Date());
//    	update(bean);
    }
	
	/**
	 * 查询--返回bean
	 * @author johnny
     * @date 2018-03-09 17:28:10
     * @Description:	
	 */
	public SsoDataDic search(SsoDataDic bean)  {

		return ssoDataDicDao.search(bean);
	}
	
}
