package com.sesame.sso.dic.bean;

import kim.sesame.framework.entity.BaseEntity;
import lombok.Data;

/**
 * SsoDataDic
 * @author johnny
 * @date 2018-03-09 17:28:10
 * @Description: 系统数据字典表
 */
@Data
public class SsoDataDic extends BaseEntity {
	private static final long serialVersionUID = 1L;

	private String typeCode;//类型编号(sys_data_dic_type.type_code)
	private String name;//名称
	private String value;//值
	private String active;//是否有效 : Y/N

    // not database field ...
	
}