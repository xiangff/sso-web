package com.sesame.sso.dic.dao;

import com.sesame.sso.dic.bean.SsoDataDic;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * SsoDataDicDao
 * @author johnny
 * @date 2018-03-09 17:28:10
 * @Description: 系统数据字典表
 */
 @Mapper
public interface SsoDataDicDao {

	/** 新增  */
	int insert(SsoDataDic bean);
	
	/** 删除  */
	int delete(String id);
	
	/** 修改  */
	int update(SsoDataDic bean);
	
	/** 查询所有 */
	List<SsoDataDic> selectList(SsoDataDic bean);
	
	/** 查询单个,返回bean */
	SsoDataDic search(SsoDataDic bean);
	

}
