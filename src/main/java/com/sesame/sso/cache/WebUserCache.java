package com.sesame.sso.cache;

import kim.sesame.framework.web.cache.IUserCache;
import kim.sesame.framework.web.entity.IUser;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@CommonsLog
@Component//(value = IUserCache.USER_LOGIN_BEAN)
public class WebUserCache implements IUserCache {

    @Autowired
    private SsoCacheServer ssoCacheServer;


    @Override
    public IUser getUserCache(String userNo) {
        return ssoCacheServer.queryUserByAccount(userNo);
    }

    @Override
    public List getUserRoles(String userNo) {
        return ssoCacheServer.queryRolesByUserno(userNo);
    }

}
