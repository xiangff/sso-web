package com.sesame.sso.cache;

import com.sesame.sso.menu.bean.SsoMenu;
import com.sesame.sso.menu.dao.SsoMenuDao;
import com.sesame.sso.role.bean.SsoRole;
import com.sesame.sso.role.dao.SsoRoleDao;
import com.sesame.sso.user.bean.SsoUser;
import com.sesame.sso.user.dao.SsoUserDao;
import kim.sesame.framework.cache.annotation.QueryCache;
import kim.sesame.framework.cache.redis.aop.QueryCacheAop;
import kim.sesame.framework.entity.GMap;
import kim.sesame.framework.web.cache.IUserCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SsoCacheServer {

    @SuppressWarnings("all")
    @Autowired
    private SsoUserDao userDao;

    @SuppressWarnings("all")
    @Autowired
    private SsoRoleDao roleDao;

    @SuppressWarnings("all")
    @Autowired
    private SsoMenuDao ssoMenuDao;


    /**
     * 根据用户编号查询角色集合
     *
     * @param userNo 用户编号
     * @return
     */
    @QueryCache(key = IUserCache.USER_ROLE_KEY)
    public List<SsoRole> queryRolesByUserno(String userNo) {
        return roleDao.queryRolesByUserNo(userNo);
    }

    public void queryRolesByUserno_invalid(String userNo) {
        String classPath = SsoCacheServer.class.toString().replace("class ", "");
        QueryCacheAop.invalid(IUserCache.USER_ROLE_KEY, null, classPath, "queryRolesByUserno", new Object[]{userNo});
    }

    /**
     * 根据用户编号查询用户对象
     *
     * @param account 用户账号
     * @return
     */
    @QueryCache(key = IUserCache.USER_INFO_KEY)
    public SsoUser queryUserByAccount(String account) {
        SsoUser bean = new SsoUser();
        bean.setAccount(account);

        return userDao.search(bean);
    }

    public void queryUserByAccount_invalid(String account) {
        String classPath = SsoCacheServer.class.toString().replace("class ", "");
        QueryCacheAop.invalid(IUserCache.USER_INFO_KEY, null, classPath, "queryUserByAccount", new Object[]{account});
    }

    @QueryCache(isWriteNullValue = false)
    public List<SsoMenu> queryMenuByUserCode(String account, String systemCode) {
        GMap params = GMap.newMap();
        params.putAction("systemCode", systemCode);
        params.putAction("account", account);

        return ssoMenuDao.getMenuByUserCode(params);
    }

    public void queryMenuByUserCode_invalid(String account, String systemCode) {
        String classPath = SsoCacheServer.class.toString().replace("class ", "");
        QueryCacheAop.invalid(null, null, classPath, "queryMenuByUserCode", new Object[]{account, systemCode});
    }

}
