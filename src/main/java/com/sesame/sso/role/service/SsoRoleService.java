package com.sesame.sso.role.service;

import com.sesame.sso.role.bean.SsoRole;
import com.sesame.sso.role.dao.SsoRoleDao;
import kim.sesame.framework.entity.GMap;
import kim.sesame.framework.entity.ZTree;
import kim.sesame.framework.utils.GData;
import kim.sesame.framework.utils.StringUtil;
import kim.sesame.framework.web.context.UserContext;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * SsoRoleService
 *
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 角色表
 */
@CommonsLog
@Service
public class SsoRoleService {

    @SuppressWarnings("all")
    @Resource
    private SsoRoleDao ssoRoleDao;

    /**
     * 查询list
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description: 分页时要注意
     */
    public List<SsoRole> searchList(SsoRole bean) {

        bean.setActive(GData.BOOLEAN.YES);
        bean.setGrade(getMaxRole().getGrade());

        List<SsoRole> list = ssoRoleDao.selectList(bean);

        return list;
    }

    public List<SsoRole> searchListAll(SsoRole bean) {

        bean.setActive(GData.BOOLEAN.YES);
//        bean.setGrade(getUserGrade());

        List<SsoRole> list = ssoRoleDao.selectListAll(bean);

        return list;
    }

    /**
     * 新增
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int add(SsoRole bean) {

        bean.setActive(GData.BOOLEAN.YES);
        bean.setNotdele(GData.BOOLEAN.YES);

        int res = ssoRoleDao.insert(bean);

        return res;
    }

    /**
     * 最大的用户角色等级的对象, 角色等级值越大,等级越小
     *
     * @return
     */
    public SsoRole getMaxRole() {
        List<SsoRole> roles = UserContext.getUserContext().getUserRole(List.class);
        SsoRole role = null;
        if (roles.size() == 1) {
            role = roles.get(0);
        } else if (roles.size() > 1) {
            List<SsoRole> list = roles.stream().sorted(Comparator.comparingInt(SsoRole::getGrade)).collect(Collectors.toList());
            role = list.get(0);
        }
        return role;
    }

    /**
     * 返回用户角色编号  'aaa','bbb'
     * @return
     */
    public String getUserRoleCodes() {
        List<SsoRole> roles = UserContext.getUserContext().getUserRole(List.class);
        List<String> roleCodes = roles.stream().map(SsoRole::getCode).collect(Collectors.toList());
        StringBuilder sb = new StringBuilder();
        for (String i : roleCodes) {
            sb.append("'").append(i).append("'").append(",");
        }
        return sb.toString().substring(0, sb.toString().length() - 1);
    }

    /**
     * 修改
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int update(SsoRole bean) {

        int res = ssoRoleDao.update(bean);

        return res;
    }

    /**
     * 删除
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int delete(String ids) {

        List<String> list_id = Stream.of(ids.split(",")).map(String::trim).distinct().filter(StringUtil::isNotEmpty).collect(Collectors.toList());
        list_id.stream().forEach(this::deleteById);

        return list_id.size();
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteById(String id) {
        //物理删除
//        ssoRoleDao.delete(id);

        //逻辑删除
        SsoRole bean = new SsoRole();
        bean.setId(id);
        bean.setActive(GData.BOOLEAN.NO);
        bean.setUpdateTime(new Date());
        update(bean);
    }

    /**
     * 查询--返回bean
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    public SsoRole search(SsoRole bean) {

        return ssoRoleDao.search(bean);
    }

    public List<ZTree> searchUserRole(String account) {
        GMap params = GMap.newMap();
        params.putAction("account", account);

        List<ZTree> list = ssoRoleDao.searchUserRole(params);
        return list;
    }

    public void saveRoleTreeData(String account, String roleCodes) {
        GMap params = GMap.newMap();
        params.putAction("account", account);

        // 1 先清除原来的数据
        ssoRoleDao.cleanUserRole(params);

        //2 保存数据
        List<String> codes = Arrays.asList(roleCodes.split(","));
        if (codes != null && codes.size() > 0) {
            params.putAction("codes", codes);

            ssoRoleDao.saveUserRole(params);
        }
    }
}
