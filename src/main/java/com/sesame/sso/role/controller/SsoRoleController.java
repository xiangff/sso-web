package com.sesame.sso.role.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sesame.sso.role.bean.SsoRole;
import com.sesame.sso.role.service.SsoRoleService;
import kim.sesame.framework.entity.GPage;
import kim.sesame.framework.entity.ZTree;
import kim.sesame.framework.mybatis.uitl.PageUtil;
import kim.sesame.framework.utils.Argument;
import kim.sesame.framework.utils.StringUtil;
import kim.sesame.framework.web.context.UserContext;
import kim.sesame.framework.web.controller.AbstractWebController;
import kim.sesame.framework.web.entity.IUser;
import kim.sesame.framework.web.response.Response;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * SsoRoleController
 *
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 角色表
 */
@CommonsLog
@RestController
@RequestMapping("/ssorole")
public class SsoRoleController extends AbstractWebController {

    @Resource
    private SsoRoleService ssoRoleService;

    /**
     * 查询list
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping("/list")
    public Response list(SsoRole bean) {

        List<SsoRole> list = ssoRoleService.searchList(bean);

        return returnSuccess(list);
    }

    /**
     * 分页查询
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @ pageNum : 页码,默认1
     * @ pageSize :  页面大小
     */
    @RequestMapping("/listPage")
    public Response listPage(SsoRole bean, GPage gPage) {

        Page<SsoRole> pages = PageHelper.startPage(gPage.getPageNum(), gPage.getPageSize())
                .doSelectPage(() -> ssoRoleService.searchList(bean));
        List<SsoRole> list = pages.getResult();

        return returnSuccess(list, PageUtil.recount(gPage, pages));
    }

    @RequestMapping("/listAll")
    public Response listAll(SsoRole bean, GPage gPage) {

        List<SsoRole> list = ssoRoleService.searchListAll(bean);

        return returnSuccess(list);
    }

    /**
     * 保存
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping(value = "/save", method = {RequestMethod.POST})
    public Response save(SsoRole bean) {
        IUser user = UserContext.getUserContext().getUser();
        int res = 0;
        if (StringUtil.isNotEmpty(bean.getId())) {
            // 主键不为空, 修改数据
            bean.initUpdate(user.getAccount());
            res = ssoRoleService.update(bean);
        } else {
            //主键为空, 新增数据
            bean.initCreateAndId(user.getAccount());
            res = ssoRoleService.add(bean);
        }
        return returnSuccess(res);
    }

    /**
     * 删除
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping("/delete")
    public Response delete(String ids, HttpServletRequest request) {
        int res = ssoRoleService.delete(ids);
        return returnSuccess(res);
    }

    /**
     * 查询详情
     *
     * @author wangjianghai
     * @date 2018-01-25 16:51:03
     * @Description:
     */
    @RequestMapping("/searchDetail")
    public Response searchDetail(SsoRole bean, HttpServletRequest request) {
        bean = ssoRoleService.search(bean);
        return returnSuccess(bean);
    }

    /**
     * 获取j角色树结构数据
     *
     * @param account 角色编号
     * @return
     */
    @RequestMapping("/findRoleTreeByAccount")
    public Response findRoleTreeByAccount(String account) {
        Argument.notEmpty(account, "用户账号 account 不能为空");

        List<ZTree> list = ssoRoleService.searchUserRole(account);

        return returnSuccess(list);
    }

    /**
     * 保存节点数据
     *
     * @param account   用户账号
     * @param roleCodes  系统节点集合,  逗号分隔
     * @return
     */
    @RequestMapping("/saveRoleTreeData")
    public Response saveRoleTreeData(String account, String roleCodes) {
        Argument.notEmpty(account, "用户账号 account 不能为空");

        ssoRoleService.saveRoleTreeData(account, roleCodes);

        return returnSuccess();
    }
}
