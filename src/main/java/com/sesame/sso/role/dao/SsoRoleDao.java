package com.sesame.sso.role.dao;

import com.sesame.sso.role.bean.SsoRole;
import kim.sesame.framework.entity.GMap;
import kim.sesame.framework.entity.ZTree;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * SsoRoleDao
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 角色表
 */
 @Mapper
public interface SsoRoleDao {

	/** 新增  */
	int insert(SsoRole bean);
	
	/** 删除  */
	int delete(String id);
	
	/** 修改  */
	int update(SsoRole bean);
	
	/** 查询list */
	List<SsoRole> selectList(SsoRole bean);
	List<SsoRole> selectListAll(SsoRole bean);
	
	/** 查询单个,返回bean */
	SsoRole search(SsoRole bean);

	List<SsoRole> queryRolesByUserNo(String value);

    List<ZTree> searchUserRole(GMap params);

	int cleanUserRole(GMap params);

	int saveUserRole(GMap params);
}
