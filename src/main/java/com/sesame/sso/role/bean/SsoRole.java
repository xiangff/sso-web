package com.sesame.sso.role.bean;

import kim.sesame.framework.utils.GData;
import kim.sesame.framework.utils.UUIDUtil;
import kim.sesame.framework.web.entity.IRole;
import lombok.Data;

import java.util.Date;

/**
 * SsoRole
 *
 * @author wangjianghai
 * @date 2018-01-25 16:51:03
 * @Description: 角色表
 */
@Data
public class SsoRole extends IRole {
    private static final long serialVersionUID = 1L;

    // not database field ...
    private String roles; // 角色集合
    private int userCount;// 用户个数

    public SsoRole() {
    }

    public SsoRole(String code, String name, Integer grade) {
        super.setCode(code);
        super.setName(name);
        super.setGrade(grade);

        setId(UUIDUtil.getShortUUID());

        Date d = new Date();
        setCreateTime(d);
        setCreateUser(GData.SYS.ADMIN_USER);
        setUpdateTime(d);
        setUpdateUser(GData.SYS.ADMIN_USER);

        super.setNotdele(GData.BOOLEAN.NO);
        super.setActive(GData.BOOLEAN.YES);
    }

}