package com.sesame.sso.menu.bean;

import lombok.Data;

@Data
public class SysFunction implements java.io.Serializable {
    private String id;
    private String name;//菜单名称
    private String key;//key或ico
}
