package com.sesame.sso.menu.bean;

import kim.sesame.framework.entity.BaseEntity;
import kim.sesame.framework.utils.GData;
import kim.sesame.framework.utils.UUIDUtil;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * SsoMenu
 *
 * @author wangjianghai
 * @date 2018-01-26 16:03:52
 * @Description: 菜单表
 */
@Data
public class SsoMenu extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private String systemCode;//系统编号,联合主键
    private String code;//菜单编号,联合主键
    private String name;//菜单名称
    private Integer grade;//菜单等级
    private String parentCode;//父菜单编号
    private String icon;//图标
    private String url;//链接
    private Integer sequence;//顺序(根据级别)
    private String notdele;//是否允许删除 : Y/N
    private String isEnable;//是否启用 : Y/N

    // not database field ...
    private String authKey; // 权限code
    List<SysFunction> list;
    private Integer count;//子菜单个数

    public SsoMenu() {
    }
    public SsoMenu(String id) {
        super.setId(id);
    }
    public SsoMenu(String name, String key, String parentCode,String systemCode) {
        this.name = name;
        this.grade = 3;
        this.parentCode = parentCode;
        this.code = key;
        this.systemCode = systemCode;
        this.isEnable = GData.BOOLEAN.YES;
    }

    public SsoMenu(String systemCode, String code, String name, String parentCode, Integer sequence) {
        this.systemCode = systemCode;
        this.code = code;
        this.name = name;
        this.grade = 3;
        this.parentCode = parentCode;
        this.sequence = sequence;
        this.notdele = GData.BOOLEAN.YES;
        this.isEnable = GData.BOOLEAN.YES;
    }

    public SsoMenu(String systemCode, String code, String name, Integer grade, String parentCode, String icon, Integer sequence, String notdele, String url) {
        this.systemCode = systemCode;
        this.code = code;
        this.name = name;
        this.grade = grade;
        this.parentCode = parentCode;
        this.icon = icon;
        this.url = url;
        this.sequence = sequence;
        this.notdele = notdele;

        setId(UUIDUtil.getShortUUID());

        Date d = new Date();
        setCreateTime(d);
        setCreateUser(GData.SYS.ADMIN_USER);
        setUpdateTime(d);
        setUpdateUser(GData.SYS.ADMIN_USER);

        this.isEnable = GData.BOOLEAN.YES;
    }
}