package com.sesame.sso.menu.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sesame.sso.menu.bean.SsoMenu;
import com.sesame.sso.menu.service.SsoMenuService;
import kim.sesame.framework.entity.GPage;
import kim.sesame.framework.entity.ZTree;
import kim.sesame.framework.mybatis.uitl.PageUtil;
import kim.sesame.framework.utils.Argument;
import kim.sesame.framework.utils.StringUtil;
import kim.sesame.framework.web.controller.AbstractWebController;
import kim.sesame.framework.web.response.Response;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * SsoMenuController
 *
 * @author wangjianghai
 * @date 2018-01-26 16:03:52
 * @Description: 菜单表
 */
@CommonsLog
@RestController
@RequestMapping("/ssomenu")
public class SsoMenuController extends AbstractWebController {

    @Resource
    private SsoMenuService ssoMenuService;

    /**
     * 查询list
     *
     * @author wangjianghai
     * @date 2018-01-26 16:03:52
     * @Description:
     */
    @RequestMapping("/list")
    public Response list(SsoMenu bean) {

        List<SsoMenu> list = ssoMenuService.searchList(bean);

        return returnSuccess(list);
    }

    /**
     * 分页查询
     *
     * @author wangjianghai
     * @date 2018-01-26 16:03:52
     * @ pageNum : 页码,默认1
     * @ pageSize :  页面大小
     */
    @RequestMapping("/listPage")
    public Response listPage(SsoMenu bean, GPage gPage) {

        Page<SsoMenu> pages = PageHelper.startPage(gPage.getPageNum(), gPage.getPageSize())
                .doSelectPage(() -> ssoMenuService.searchList(bean));
        List<SsoMenu> list = pages.getResult();

        return returnSuccess(list, PageUtil.recount(gPage, pages));
    }

    /**
     * 保存
     *
     * @author wangjianghai
     * @date 2018-01-26 16:03:52
     * @Description:
     */
    @RequestMapping(value = "/save", method = {RequestMethod.POST})
    public Response save(SsoMenu bean) {
        //IUser user = UserContext.getUserContext().getUser();
        int res = 0;
        if (StringUtil.isNotEmpty(bean.getId())) {
            // 主键不为空, 修改数据
            bean.initUpdate("");
            res = ssoMenuService.update(bean);
        } else {
            //主键为空, 新增数据
            bean.initCreateAndId("");
            res = ssoMenuService.add(bean);
        }
        return returnSuccess(res);
    }
    @RequestMapping(value = "/updateState", method = {RequestMethod.POST})
    public Response updateState(SsoMenu bean) {

        int res = ssoMenuService.updateState(bean);
        return returnSuccess(res);
    }

    /**
     * 删除
     *
     * @author wangjianghai
     * @date 2018-01-26 16:03:52
     * @Description:
     */
    @RequestMapping("/delete")
    public Response delete(String ids, HttpServletRequest request) {
        int res = ssoMenuService.delete(ids);
        return returnSuccess(res);
    }

    /**
     * 查询详情
     *
     * @author wangjianghai
     * @date 2018-01-26 16:03:52
     * @Description:
     */
    @RequestMapping("/searchDetail")
    public Response searchDetail(SsoMenu bean, HttpServletRequest request) {
        bean = ssoMenuService.search(bean);
        return returnSuccess(bean);
    }

    /**
     * 获取菜单树结构数据
     *
     * @param systemCode 系统编号
     * @param roleCode   角色编号
     * @return
     */
    @RequestMapping("/findMenuTreeByCode")
    public Response findMenuTreeByCode(String systemCode, String roleCode) {
        Argument.notEmpty(systemCode, "系统编号 systemCode 不能为空");
        Argument.notEmpty(roleCode, "角色编号 code 不能为空");

        List<ZTree> list = ssoMenuService.searchRoleMenu(systemCode, roleCode);

        return returnSuccess(list);
    }

    /**
     * 保存节点数据
     *
     * @param systemCode 系统编号
     * @param roleCode   角色编号
     * @param menuCodes  菜单节点集合,  逗号分隔
     * @return
     */
    @RequestMapping("/saveMenuTreeData")
    public Response saveMenuTreeData(String systemCode, String roleCode, String menuCodes) {
        Argument.notEmpty(systemCode, "系统编号 systemCode 不能为空");
        Argument.notEmpty(roleCode, "角色编号 code 不能为空");

        ssoMenuService.saveMenuTreeData(systemCode, roleCode, menuCodes);

        return returnSuccess();
    }

}
