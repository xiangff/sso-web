package com.sesame.sso.menu.dao;

import com.sesame.sso.menu.bean.SsoMenu;
import com.sesame.sso.menu.bean.SysFunction;
import kim.sesame.framework.entity.GMap;
import kim.sesame.framework.entity.ZTree;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * SsoMenuDao
 * @author wangjianghai
 * @date 2018-01-26 16:03:52
 * @Description: 菜单表
 */
 @Mapper
public interface SsoMenuDao {

	/** 新增  */
	int insert(SsoMenu bean);
	
	/** 删除  */
	int delete(String id);
	
	/** 修改  */
	int update(SsoMenu bean);
	
	/** 查询list */
	List<SsoMenu> selectList(SsoMenu bean);
	
	/** 查询单个,返回bean */
	SsoMenu search(SsoMenu bean);

    List<SsoMenu> getMenuByUserCode(GMap params);

    List<ZTree> searchRoleMenu(GMap params);

	int cleanRoleMenu(GMap params);

	int saveRoleMenu(GMap params);

    int initInsertAdmin(GMap params);

    List<SysFunction> searchChildMenu(SsoMenu resBean);
    List<SsoMenu> searchMenu(SsoMenu resBean);

	int initDeleteMenu(GMap params);

	int updateDelete(GMap params);

	int deleteRoleMenu(SsoMenu bean);
}
