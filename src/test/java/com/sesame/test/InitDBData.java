package com.sesame.test;

import com.sesame.AppSSO;
import com.sesame.sso.menu.bean.SsoMenu;
import com.sesame.sso.menu.dao.SsoMenuDao;
import com.sesame.sso.role.bean.SsoRole;
import com.sesame.sso.role.dao.SsoRoleDao;
import com.sesame.sso.user.bean.SsoUser;
import com.sesame.sso.user.dao.SsoUserDao;
import kim.sesame.framework.utils.GData;
import kim.sesame.framework.utils.MD5Util;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Arrays;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppSSO.class)
public class InitDBData {

    @SuppressWarnings("all")
    @Resource
    private SsoUserDao ssoUserDao;

    @SuppressWarnings("all")
    @Resource
    private SsoRoleDao ssoRoleDao;

    @SuppressWarnings("all")
    @Resource
    private SsoMenuDao ssoMenuDao;

    @Test
    public void Test() {

//        user();
//        role();
//        menu();
        update_menu();
    }

    private void update_menu() {
        String systemCode = "sso";
        Arrays.asList(
                newSsoMenu(systemCode, "user_set", "page/sso~user~ssouser_list")
                ,newSsoMenu(systemCode, "menu_set", "page/sso~menu~ssomenu_list")
                ,newSsoMenu(systemCode, "system_set", "page/sso~system~ssosystem_list")
                ,newSsoMenu(systemCode, "role_set", "page/sso~role~ssorole_list")
                ,newSsoMenu(systemCode, "data_dic", "page/sso~dic~ssodatadic_list")
        ).stream().forEach(ssoMenuDao::update);
    }
    public SsoMenu newSsoMenu(String systemCode, String code, String url) {
        SsoMenu bean = new SsoMenu();
        bean.setSystemCode(systemCode);
        bean.setCode(code);
        bean.setUrl(url);
        return  bean;
    }

    private void menu() {
        String systemCode = "sso";
        Arrays.asList(
                new SsoMenu(systemCode, "sys_centre", "系统中心", 1, null, "&#xe614;", 1, "N", "url")
                , new SsoMenu(systemCode, "div_doc", "开发文档", 1, null, "&#xe613;", 2, "N", "url")


                , new SsoMenu(systemCode, "user_set", "用户管理", 2, "sys_centre", "&#xe613;", 1, "N", "url")
                /**/, new SsoMenu(systemCode, "user_set_search", "查询", 3, "user_set", null, 1, "N", null)
                /**/, new SsoMenu(systemCode, "user_set_add", "新增", 3, "user_set", null, 2, "N", null)
                /**/, new SsoMenu(systemCode, "user_set_update", "修改", 3, "user_set", null, 3, "N", null)
                /**/, new SsoMenu(systemCode, "user_set_delete", "删除", 3, "user_set", null, 4, "N", null)
                /**/, new SsoMenu(systemCode, "user_set_role", "编辑角色", 3, "user_set", null, 5, "N", null)

                , new SsoMenu(systemCode, "role_set", "角色管理", 2, "sys_centre", "&#xe609;", 2, "N", "url")
                 /**/, new SsoMenu(systemCode, "role_set_search", "查询", 3, "role_set", null, 1, "N", null)
                /**/, new SsoMenu(systemCode, "role_set_add", "新增", 3, "role_set", null, 2, "N", null)
                /**/, new SsoMenu(systemCode, "role_set_update", "修改", 3, "role_set", null, 3, "N", null)
                /**/, new SsoMenu(systemCode, "role_set_delete", "删除", 3, "role_set", null, 4, "N", null)
                /**/, new SsoMenu(systemCode, "role_set_menu", "菜单权限", 3, "role_set", null, 5, "N", null)
                /**/, new SsoMenu(systemCode, "role_set_system", "系统权限", 3, "role_set", null, 6, "N", null)

                , new SsoMenu(systemCode, "data_dic", "数据字典", 2, "sys_centre", "&#xe631;", 3, "N", "url")
                  /**/, new SsoMenu(systemCode, "data_dic_search", "查询", 3, "data_dic", null, 1, "N", null)
                /**/, new SsoMenu(systemCode, "data_dic_add", "新增", 3, "data_dic", null, 2, "N", null)
                /**/, new SsoMenu(systemCode, "data_dic_update", "修改", 3, "data_dic", null, 3, "N", null)
                /**/, new SsoMenu(systemCode, "data_dic_delete", "删除", 3, "data_dic", null, 4, "N", null)

                , new SsoMenu(systemCode, "system_set", "系统管理", 2, "sys_centre", "&#xe631;", 4, "N", "url")
                   /**/, new SsoMenu(systemCode, "system_set_search", "查询", 3, "system_set", null, 1, "N", null)
                /**/, new SsoMenu(systemCode, "system_set_add", "新增", 3, "system_set", null, 2, "N", null)
                /**/, new SsoMenu(systemCode, "system_set_update", "修改", 3, "system_set", null, 3, "N", null)
                /**/, new SsoMenu(systemCode, "system_set_delete", "删除", 3, "system_set", null, 4, "N", null)

                , new SsoMenu(systemCode, "menu_set", "菜单管理", 2, "sys_centre", "&#xe647;", 5, "N", "url")
                /**/, new SsoMenu(systemCode, "menu_set_search", "查询", 3, "menu_set", null, 1, "N", null)
                /**/, new SsoMenu(systemCode, "menu_set_add", "新增", 3, "menu_set", null, 2, "N", null)
                /**/, new SsoMenu(systemCode, "menu_set_update", "修改", 3, "menu_set", null, 3, "N", null)
                /**/, new SsoMenu(systemCode, "menu_set_delete", "删除", 3, "menu_set", null, 4, "N", null)

                , new SsoMenu(systemCode, "doc_1", "文档1", 2, "div_doc", "&#xe647;", 1, "N", "url")

        ).stream().forEach(ssoMenuDao::insert);
    }

    public void user() {
        Arrays.asList(
                new SsoUser(GData.SYS.SPUER_ADMIN_USER, GData.SYS.SPUER_ADMIN_ROLE, "spuerManage", MD5Util.encodeByMD5("123456"))
                , new SsoUser(GData.SYS.ADMIN_USER, GData.SYS.ADMIN_ROLE, "admin", MD5Util.encodeByMD5("123456"))
                , new SsoUser("johnny", GData.SYS.ADMIN_ROLE, "jianghai", MD5Util.encodeByMD5("123456"))
        ).stream().forEach(ssoUserDao::insert);
    }

    public void role() {
        Arrays.asList(
                new SsoRole(GData.SYS.SPUER_ADMIN_ROLE, "超级管理员", 1)
                , new SsoRole(GData.SYS.ADMIN_ROLE, "管理员", 2)
        ).stream().forEach(ssoRoleDao::insert);
    }


}