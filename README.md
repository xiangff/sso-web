# sso-web 
通用后台, 管理用户,角色,系统,菜单,单点登录,公用数据字典.作为分布式系统管理的系统
### [文档地址](https://gitee.com/sesamekim/framework-boot/wikis/pages?title=1--%E5%BA%8F&parent=%E9%A1%B5%E9%9D%A2%E7%BC%96%E5%86%99%E8%A7%84%E5%88%99)

#### 其他项目地址
- framework-parent : [https://gitee.com/sesamekim/framework-boot](https://gitee.com/sesamekim/framework-boot)
- demo 项目地址 : [https://gitee.com/sesamekim/demo](https://gitee.com/sesamekim/demo)
- 代码生成工具 : [https://gitee.com/sesamekim/codeGenerate](https://gitee.com/sesamekim/codeGenerate)